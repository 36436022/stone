/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50722
 Source Host           : localhost:3306
 Source Schema         : stone

 Target Server Type    : MySQL
 Target Server Version : 50722
 File Encoding         : 65001

 Date: 31/12/2019 10:15:47
*/

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `blob_data` blob NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `calendar_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `calendar` blob NOT NULL,
  PRIMARY KEY (`sched_name`, `calendar_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `cron_expression` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time_zone_id` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('AKScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', '0/10 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('AKScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', '0/15 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('AKScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', '0/20 * * * * ?', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `entry_id` varchar(95) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `instance_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fired_time` bigint(13) NOT NULL,
  `sched_time` bigint(13) NOT NULL,
  `priority` int(11) NOT NULL,
  `state` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `requests_recovery` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `entry_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `job_class_name` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_durable` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_update_data` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `requests_recovery` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_data` blob NULL,
  PRIMARY KEY (`sched_name`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('AKScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 'com.ak.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001B636F6D2E616B2E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720024636F6D2E616B2E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200084C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C000764656C466C616771007E00094C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001622CDE29E07870707400007070707400013174000E302F3130202A202A202A202A203F7400116D795461736B2E72734E6F506172616D7374000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000001740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E697A0E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('AKScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 'com.ak.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001B636F6D2E616B2E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720024636F6D2E616B2E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200084C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C000764656C466C616771007E00094C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001622CDE29E07870707400007070707400013174000E302F3135202A202A202A202A203F7400156D795461736B2E7279506172616D7328277279272974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000002740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E69C89E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('AKScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 'com.ak.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001B636F6D2E616B2E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720024636F6D2E616B2E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200084C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C000764656C466C616771007E00094C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001622CDE29E07870707400007070707400013174000E302F3230202A202A202A202A203F7400386D795461736B2E72794D756C7469706C65506172616D7328277279272C20747275652C20323030304C2C203331362E3530442C203130302974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000003740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E5A49AE58F82EFBC8974000133740001317800);

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lock_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`sched_name`, `lock_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('AKScheduler', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('AKScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`sched_name`, `trigger_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `instance_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `last_checkin_time` bigint(13) NOT NULL,
  `checkin_interval` bigint(13) NOT NULL,
  PRIMARY KEY (`sched_name`, `instance_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
INSERT INTO `qrtz_scheduler_state` VALUES ('AKScheduler', 'DESKTOP-3I5I6QN1577689660211', 1577695575236, 15000);

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `repeat_count` bigint(7) NOT NULL,
  `repeat_interval` bigint(12) NOT NULL,
  `times_triggered` bigint(10) NOT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `str_prop_1` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `str_prop_2` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `str_prop_3` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `int_prop_1` int(11) NULL DEFAULT NULL,
  `int_prop_2` int(11) NULL DEFAULT NULL,
  `long_prop_1` bigint(20) NULL DEFAULT NULL,
  `long_prop_2` bigint(20) NULL DEFAULT NULL,
  `dec_prop_1` decimal(13, 4) NULL DEFAULT NULL,
  `dec_prop_2` decimal(13, 4) NULL DEFAULT NULL,
  `bool_prop_1` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bool_prop_2` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `next_fire_time` bigint(13) NULL DEFAULT NULL,
  `prev_fire_time` bigint(13) NULL DEFAULT NULL,
  `priority` int(11) NULL DEFAULT NULL,
  `trigger_state` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `trigger_type` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `start_time` bigint(13) NOT NULL,
  `end_time` bigint(13) NULL DEFAULT NULL,
  `calendar_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `misfire_instr` smallint(2) NULL DEFAULT NULL,
  `job_data` blob NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  INDEX `sched_name`(`sched_name`, `job_name`, `job_group`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('AKScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 1577689660000, -1, 5, 'PAUSED', 'CRON', 1577689660000, 0, NULL, 2, '');
INSERT INTO `qrtz_triggers` VALUES ('AKScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 1577689665000, -1, 5, 'PAUSED', 'CRON', 1577689661000, 0, NULL, 2, '');
INSERT INTO `qrtz_triggers` VALUES ('AKScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 1577689680000, -1, 5, 'PAUSED', 'CRON', 1577689661000, 0, NULL, 2, '');

-- ----------------------------
-- Table structure for sys_application
-- ----------------------------
DROP TABLE IF EXISTS `sys_application`;
CREATE TABLE `sys_application`  (
  `app_code` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '应用编号',
  `app_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `app_uri` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '应用访问地址',
  `app_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '类型（0：内置子系统；1：租户子系统）',
  `app_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '应用部署地址',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '在线状态（0在线 1离线）',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序（升序）',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注信息',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标记（0代表存在 2代表删除）',
  PRIMARY KEY (`app_code`) USING BTREE,
  UNIQUE INDEX `UNIQUE_APP_CODE`(`app_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '应用监控表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_application
-- ----------------------------
INSERT INTO `sys_application` VALUES ('PlatformMonitor', '统一监控子系统', 'localhost', '0', '11', '0', NULL, '', 'admin', '2019-10-21 11:17:03', 'admin', '2019-10-26 11:34:36', '0');
INSERT INTO `sys_application` VALUES ('PlatformTenant', '平台租户管理', 'localhost', '0', '/home/wwwweb/tenant3', '0', 0, '', 'admin', '2019-07-26 14:24:48', 'admin', '2019-10-25 12:40:58', '0');
INSERT INTO `sys_application` VALUES ('TenantApi', '平台开放接口', 'localhost', '1', '', '0', 4, '', 'admin', '2019-08-02 15:11:00', 'admin', '2019-11-26 14:52:35', '0');
INSERT INTO `sys_application` VALUES ('TenantBasedate', '基础数据子系统', 'localhost', '1', '/home/wwwweb/basedata', '0', 3, '', 'admin', '2019-07-26 15:00:16', 'admin', '2019-11-26 19:10:27', '0');
INSERT INTO `sys_application` VALUES ('TenantLog', '日志查询子系统', 'localhost', '1', 'localhost', '1', NULL, '', 'admin', '2019-10-25 21:05:37', 'admin', '2019-12-17 22:26:16', '0');
INSERT INTO `sys_application` VALUES ('TenantMessage', '消息中心子系统', 'localhost', '1', 'localhost', '0', NULL, '', 'admin', '2019-10-25 21:07:17', NULL, NULL, '0');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '参数配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2018-03-16 11:33:00', 'admin', '2019-12-27 09:25:41', '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '初始化密码 123456');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '深黑主题theme-dark，浅色主题theme-light，深蓝主题theme-blue');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` int(16) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` int(16) NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `tenant_code` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '000000' COMMENT '所属租户',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 220 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', '云药科技', '000000', 0, 'vean', '15888888888', '', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (101, 200, '0,200', '广州总公司', '000000', 1, 'vean', '15888888888', '', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2019-11-26 10:26:12');
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', '长沙分公司', '000000', 2, 'vean', '15888888888', '', '0', '2', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (103, 101, '0,200,101', '研发部门', '000000', 1, 'vean', '15888888888', '', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (104, 101, '0,200,101', '市场部门', '000000', 2, 'vean', '15888888888', '', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (105, 101, '0,200,101', '测试部门', '000000', 3, 'vean', '15888888888', '', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (106, 101, '0,200,101', '财务部门', '000000', 0, 'vean', '15888888888', '', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2019-11-26 10:26:12');
INSERT INTO `sys_dept` VALUES (107, 101, '0,200,101', '运维部门', '000000', 5, 'vean', '15888888888', '', '0', '2', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (108, 102, '0,100,102', '市场部门', '000000', 1, 'vean', '15888888888', '', '0', '2', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (109, 102, '0,100,102', '财务部门', '000000', 2, 'vean', '15888888888', '', '0', '2', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (200, 0, '0', '昆仑科技', '222222', 0, '赵总', '16666666666', '', '0', '0', 'admin', '2019-08-05 10:45:28', 'admin', '2019-11-26 10:26:12');
INSERT INTO `sys_dept` VALUES (202, 0, '0', '振康物流', '111111', 0, '张总', '18888888888', '', '0', '0', 'admin', '2019-08-06 18:23:53', '', NULL);
INSERT INTO `sys_dept` VALUES (203, 202, '0,202', '研发部门', '111111', 2, '邓海斌', NULL, NULL, '0', '0', '111111_admin', '2019-08-13 09:55:39', '', NULL);
INSERT INTO `sys_dept` VALUES (204, 200, '0,200', '研发部门', '222222', 1, '张三', '18688387621', NULL, '0', '0', '222222_admin', '2019-08-14 11:01:40', '', NULL);
INSERT INTO `sys_dept` VALUES (207, 203, '0,202,203', '111', '111111', 111, '111', '18688387621', '22658318@qq.com', '0', '0', 'test', '2019-10-23 20:17:35', '', NULL);
INSERT INTO `sys_dept` VALUES (209, 0, '', '111-1', '111111', 1, '11111', '13793228039', NULL, '0', '2', 't1', '2019-10-24 16:20:00', '', NULL);
INSERT INTO `sys_dept` VALUES (210, 0, '', 'test1', '111111', 1, '13693228049', NULL, NULL, '0', '2', 't1', '2019-10-24 16:33:45', '', NULL);
INSERT INTO `sys_dept` VALUES (212, 0, '0', '租户试验平台', 'uDLBWt', 0, '碧海', '18653140396', NULL, '0', '0', 'admin', '2019-11-25 11:35:32', '', NULL);
INSERT INTO `sys_dept` VALUES (213, 104, '0,200,101,104', '基2', '000000', 1, '223', '13699999999', '212112@qq.com', '0', '0', 'admin', '2019-11-26 10:06:22', '', NULL);
INSERT INTO `sys_dept` VALUES (214, 0, '', 'a', '000000', 1, 'a', '', '', '1', '0', 'admin', '2019-11-26 10:27:49', 'admin', '2019-11-26 10:30:33');
INSERT INTO `sys_dept` VALUES (215, 0, '', 'b', '000000', 1, '', '', '', '1', '2', 'admin', '2019-11-26 10:29:35', 'admin', '2019-11-26 10:30:45');
INSERT INTO `sys_dept` VALUES (216, 0, '0', '厦门新星科技', 'xtvXeH', 0, 'Jack', '18288888888', NULL, '0', '0', 'admin', '2019-12-06 17:01:01', '', NULL);
INSERT INTO `sys_dept` VALUES (217, 0, '', 'aaa', '000000', 1, 'glw', '13764935656', '651885968@qq.com', '0', '0', 'admin', '2019-12-07 17:25:30', '', NULL);
INSERT INTO `sys_dept` VALUES (218, 0, '', '天眼公司', '000000', 23, '23', NULL, NULL, '0', '0', 'admin', '2019-12-17 10:25:55', '', NULL);
INSERT INTO `sys_dept` VALUES (219, 0, '0', 'AIBDGO', 'ElRetC', 0, '严荣兴', '13699132623', NULL, '0', '0', 'admin', '2019-12-28 17:47:11', '', NULL);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 104 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '新增操作');
INSERT INTO `sys_dict_data` VALUES (19, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '修改操作');
INSERT INTO `sys_dict_data` VALUES (20, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '删除操作');
INSERT INTO `sys_dict_data` VALUES (21, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '授权操作');
INSERT INTO `sys_dict_data` VALUES (22, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '导出操作');
INSERT INTO `sys_dict_data` VALUES (23, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '导入操作');
INSERT INTO `sys_dict_data` VALUES (24, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '强退操作');
INSERT INTO `sys_dict_data` VALUES (25, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '生成操作');
INSERT INTO `sys_dict_data` VALUES (26, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '清空操作');
INSERT INTO `sys_dict_data` VALUES (27, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES (28, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '停用状态');
INSERT INTO `sys_dict_data` VALUES (29, 0, '内置', '0', 'sys_application_type', NULL, 'default', 'Y', '0', 'admin', '2019-08-02 11:03:18', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (30, 1, '租户', '1', 'sys_application_type', NULL, 'default', 'Y', '0', 'admin', '2019-08-02 11:03:32', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (100, 0, '主库', '0', 'sys_database_flag', '', 'default', 'Y', '0', 'admin', '2019-08-01 14:45:50', 'admin', '2019-08-01 15:17:16', '默认情况选择一个库，考虑到今后可能读写分离，或者配置主从');
INSERT INTO `sys_dict_data` VALUES (101, 1, '从库', '1', 'sys_database_flag', '', 'default', 'Y', '0', 'admin', '2019-08-01 14:47:03', 'admin', '2019-08-01 15:17:12', '');
INSERT INTO `sys_dict_data` VALUES (102, 0, 'MySQL', 'com.mysql.cj.jdbc.Driver', 'sys_database_type', '', 'default', 'Y', '0', 'admin', '2019-08-02 13:38:33', 'admin', '2019-12-05 10:17:34', '');
INSERT INTO `sys_dict_data` VALUES (103, 1, 'Oracle', 'oracle.jdbc.driver.OracleDriver', 'sys_database_type', '', 'default', 'Y', '0', 'admin', '2019-08-02 13:38:54', 'admin', '2019-09-24 13:35:03', '');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 102 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '登录状态列表');
INSERT INTO `sys_dict_type` VALUES (11, '子系统类型', 'sys_application_type', '0', 'admin', '2019-08-02 11:02:43', 'admin', '2019-08-06 16:50:53', '0内置，1租户');
INSERT INTO `sys_dict_type` VALUES (100, '数据源标识', 'sys_database_flag', '0', 'admin', '2019-08-01 14:43:34', 'admin', '2019-12-05 10:56:05', '数据源标识');
INSERT INTO `sys_dict_type` VALUES (101, '数据库类型', 'sys_database_type', '0', 'admin', '2019-08-02 13:37:13', 'admin', '2019-08-02 13:37:55', '数据库类型（Oracle、MySQL）');

-- ----------------------------
-- Table structure for sys_districts
-- ----------------------------
DROP TABLE IF EXISTS `sys_districts`;
CREATE TABLE `sys_districts`  (
  `id` int(6) UNSIGNED NOT NULL COMMENT '编号',
  `pid` int(6) NOT NULL DEFAULT 0 COMMENT '上级编号',
  `deep` tinyint(1) NOT NULL DEFAULT 0 COMMENT '层级',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '名称',
  `pinyin` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '拼音',
  `pinyin_shor` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '拼音缩写',
  `ext_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '扩展名',
  `create_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '地区' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_districts
-- ----------------------------
INSERT INTO `sys_districts` VALUES (1111, 11, 0, '111', '111', '11', '111', 'admin', '2019-11-26 16:01:18', NULL, NULL);

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务调度表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'myTask.rsNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2018-03-16 11:33:00', 'admin', '2019-12-18 19:28:44', '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'myTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2018-03-16 11:33:00', 'admin', '2019-12-13 14:36:26', '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'myTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2018-03-16 11:33:00', 'admin', '2019-10-24 11:48:26', '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '异常信息',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 70961 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `login_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录账号',
  `ipaddr` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '提示消息',
  `login_time` datetime(0) NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 655 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统访问记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` int(16) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
  `app_code` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '子系统Code',
  `parent_id` int(16) NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '#' COMMENT '请求地址',
  `target` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '打开方式（menuItem页签 menuBlank新窗口）',
  `menu_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `perms` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2013 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '租户管理', 'PlatformTenant', 0, 1, '#', 'menuItem', 'M', '0', '', 'fa fa-gear', 'admin', '2018-03-16 11:33:00', 'admin', '2019-11-26 15:28:09', '');
INSERT INTO `sys_menu` VALUES (2, '统一监控', 'PlatformMonitor', 0, 2, '#', 'menuItem', 'M', '0', '', 'fa fa-video-camera', 'admin', '2018-03-16 11:33:00', 'admin', '2019-08-07 10:58:56', '');
INSERT INTO `sys_menu` VALUES (3, '开放接口', 'TenantApi', 0, 4, '#', 'menuItem', 'M', '0', '', 'fa fa-bars', 'admin', '2018-03-16 11:33:00', 'admin', '2019-08-07 10:59:06', '');
INSERT INTO `sys_menu` VALUES (4, '任务调度', 'PlatformQuartz', 0, 3, '#', 'menuItem', 'M', '0', '', 'fa fa-bars', 'admin', '2019-07-30 10:50:44', 'admin', '2019-08-07 10:59:01', '');
INSERT INTO `sys_menu` VALUES (5, '日志查询', 'TenantLog', 0, 7, '#', 'menuItem', 'M', '0', '', 'fa fa-gear', 'admin', '2018-03-16 11:33:00', 'admin', '2019-11-26 14:30:40', '日志管理菜单');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 'TenantBasedate', 2000, 1, '/basedata/user', '', 'C', '0', 'basedata:user:view', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 'TenantBasedate', 2000, 2, '/basedata/role', '', 'C', '0', 'basedata:role:view', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 'TenantBasedate', 2000, 4, '/basedata/menu', '', 'C', '0', 'basedata:menu:view', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 'TenantBasedate', 2000, 4, '/basedata/dept', '', 'C', '0', 'basedata:dept:view', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 'TenantBasedate', 2000, 5, '/basedata/post', '', 'C', '0', 'basedata:post:view', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 'TenantBasedate', 2000, 6, '/system/dict', '', 'C', '0', 'system:dict:view', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 'TenantBasedate', 2000, 7, '/system/config', '', 'C', '0', 'system:config:view', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 'TenantMessage', 2001, 8, '/message/notice', '', 'C', '0', 'message:notice:view', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '通知公告菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 'PlatformMonitor', 2, 1, '/monitor/online', '', 'C', '0', 'monitor:online:view', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 'PlatformQuartz', 4, 2, '/monitor/job', '', 'C', '0', 'monitor:job:view', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, '数据监控', 'PlatformMonitor', 2, 3, '/monitor/data', '', 'C', '0', 'monitor:data:view', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '数据监控菜单');
INSERT INTO `sys_menu` VALUES (112, '服务监控', 'PlatformMonitor', 2, 3, '/monitor/server', '', 'C', '0', 'monitor:server:view', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '服务监控菜单');

INSERT INTO `sys_menu` VALUES (115, '系统接口', 'TenantApi', 3, 3, '/tool/swagger', '', 'C', '0', 'tool:swagger:view', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 'TenantLog', 5, 1, '/log/operlog', '', 'C', '0', 'log:operlog:view', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 'TenantLog', 5, 2, '/log/logininfor', '', 'C', '0', 'log:logininfor:view', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1000, '用户查询', 'TenantBasedate', 100, 1, '#', '', 'F', '0', 'basedata:user:list', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1001, '用户新增', 'TenantBasedate', 100, 2, '#', '', 'F', '0', 'basedata:user:add', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1002, '用户修改', 'TenantBasedate', 100, 3, '#', '', 'F', '0', 'basedata:user:edit', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1003, '用户删除', 'TenantBasedate', 100, 4, '#', '', 'F', '0', 'basedata:user:remove', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1004, '用户导出', 'TenantBasedate', 100, 5, '#', '', 'F', '0', 'basedata:user:export', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1005, '用户导入', 'TenantBasedate', 100, 6, '#', '', 'F', '0', 'basedata:user:import', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1006, '重置密码', 'TenantBasedate', 100, 7, '#', '', 'F', '0', 'basedata:user:resetPwd', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1007, '角色查询', 'TenantBasedate', 101, 1, '#', '', 'F', '0', 'basedata:role:list', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1008, '角色新增', 'TenantBasedate', 101, 2, '#', '', 'F', '0', 'basedata:role:add', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1009, '角色修改', 'TenantBasedate', 101, 3, '#', '', 'F', '0', 'basedata:role:edit', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1010, '角色删除', 'TenantBasedate', 101, 4, '#', '', 'F', '0', 'basedata:role:remove', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1011, '角色导出', 'TenantBasedate', 101, 5, '#', '', 'F', '0', 'basedata:role:export', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1012, '菜单查询', 'TenantBasedate', 102, 1, '#', '', 'F', '0', 'basedata:menu:list', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1013, '菜单新增', 'TenantBasedate', 102, 2, '#', '', 'F', '0', 'basedata:menu:add', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1014, '菜单修改', 'TenantBasedate', 102, 3, '#', '', 'F', '0', 'basedata:menu:edit', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1015, '菜单删除', 'TenantBasedate', 102, 4, '#', '', 'F', '0', 'basedata:menu:remove', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1016, '部门查询', 'TenantBasedate', 103, 1, '#', '', 'F', '0', 'basedata:dept:list', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1017, '部门新增', 'TenantBasedate', 103, 2, '#', '', 'F', '0', 'basedata:dept:add', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1018, '部门修改', 'TenantBasedate', 103, 3, '#', '', 'F', '0', 'basedata:dept:edit', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1019, '部门删除', 'TenantBasedate', 103, 4, '#', '', 'F', '0', 'basedata:dept:remove', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1020, '岗位查询', 'TenantBasedate', 104, 1, '#', '', 'F', '0', 'basedata:post:list', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1021, '岗位新增', 'TenantBasedate', 104, 2, '#', '', 'F', '0', 'basedata:post:add', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1022, '岗位修改', 'TenantBasedate', 104, 3, '#', '', 'F', '0', 'basedata:post:edit', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1023, '岗位删除', 'TenantBasedate', 104, 4, '#', '', 'F', '0', 'basedata:post:remove', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1024, '岗位导出', 'TenantBasedate', 104, 5, '#', '', 'F', '0', 'basedata:post:export', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1025, '字典查询', 'TenantBasedate', 105, 1, '#', '', 'F', '0', 'system:dict:list', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1026, '字典新增', 'TenantBasedate', 105, 2, '#', '', 'F', '0', 'system:dict:add', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1027, '字典修改', 'TenantBasedate', 105, 3, '#', '', 'F', '0', 'system:dict:edit', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1028, '字典删除', 'TenantBasedate', 105, 4, '#', '', 'F', '0', 'system:dict:remove', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1029, '字典导出', 'TenantBasedate', 105, 5, '#', '', 'F', '0', 'system:dict:export', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1030, '参数查询', 'TenantBasedate', 106, 1, '#', '', 'F', '0', 'system:config:list', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1031, '参数新增', 'TenantBasedate', 106, 2, '#', '', 'F', '0', 'system:config:add', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1032, '参数修改', 'TenantBasedate', 106, 3, '#', '', 'F', '0', 'system:config:edit', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1033, '参数删除', 'TenantBasedate', 106, 4, '#', '', 'F', '0', 'system:config:remove', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1034, '参数导出', 'TenantBasedate', 106, 5, '#', '', 'F', '0', 'system:config:export', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1035, '公告查询', 'TenantMessage', 107, 1, '#', 'menuItem', 'F', '0', 'message444:notice:list', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2019-11-26 15:29:11', '');
INSERT INTO `sys_menu` VALUES (1036, '公告新增', 'TenantMessage', 107, 2, '#', '', 'F', '0', 'message:notice:add', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1037, '公告修改', 'TenantMessage', 107, 3, '#', '', 'F', '0', 'message:notice:edit', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1038, '公告删除', 'TenantMessage', 107, 4, '#', 'menuItem', 'F', '0', 'message:notice:remove444', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2019-11-26 14:35:09', '');
INSERT INTO `sys_menu` VALUES (1039, '操作查询', 'TenantLog', 500, 1, '#', '', 'F', '0', 'log:operlog:list', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1040, '操作删除', 'TenantLog', 500, 2, '#', '', 'F', '0', 'log:operlog:remove', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1041, '详细信息', 'TenantLog', 500, 3, '#', '', 'F', '0', 'log:operlog:detail', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1042, '日志导出', 'TenantLog', 500, 4, '#', '', 'F', '0', 'log:operlog:export', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1043, '登录查询', 'TenantLog', 501, 1, '#', '', 'F', '0', 'log:logininfor:list', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1044, '登录删除', 'TenantLog', 501, 2, '#', '', 'F', '0', 'log:logininfor:remove', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1045, '日志导出', 'TenantLog', 501, 3, '#', '', 'F', '0', 'log:logininfor:export', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1046, '在线查询', 'PlatformMonitor', 109, 1, '#', '', 'F', '0', 'monitor:online:list', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1047, '批量强退', 'PlatformMonitor', 109, 2, '#', '', 'F', '0', 'monitor:online:batchForceLogout', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1048, '单条强退', 'PlatformMonitor', 109, 3, '#', '', 'F', '0', 'monitor:online:forceLogout', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1049, '任务查询', 'PlatformQuartz', 110, 1, '#', '', 'F', '0', 'monitor:job:list', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1050, '任务新增', 'PlatformQuartz', 110, 2, '#', '', 'F', '0', 'monitor:job:add', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1051, '任务修改', 'PlatformQuartz', 110, 3, '#', '', 'F', '0', 'monitor:job:edit', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1052, '任务删除', 'PlatformQuartz', 110, 4, '#', '', 'F', '0', 'monitor:job:remove', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1053, '状态修改', 'PlatformQuartz', 110, 5, '#', '', 'F', '0', 'monitor:job:changeStatus', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1054, '任务详细', 'PlatformQuartz', 110, 6, '#', '', 'F', '0', 'monitor:job:detail', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1055, '任务导出', 'PlatformQuartz', 110, 7, '#', '', 'F', '0', 'monitor:job:export', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1056, '生成查询', 'PlatformTenant', 114, 1, '#', '', 'F', '0', 'tool:gen:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1057, '生成修改', 'PlatformTenant', 114, 2, '#', '', 'F', '0', 'tool:gen:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1058, '生成删除', 'PlatformTenant', 114, 3, '#', '', 'F', '0', 'tool:gen:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1059, '租户管理', 'PlatformTenant', 1, 2, '/system/tenant', 'menuItem', 'C', '0', 'system:tenant:view', '#', 'admin', '2019-07-31 17:26:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1060, '子系统管理', 'PlatformTenant', 1, 1, '/system/application', 'menuItem', 'C', '0', 'system:application:view', '#', 'admin', '2019-07-31 17:26:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1062, '账户解锁', 'TenantLog', 501, 4, '#', '', 'F', '0', 'monitor:logininfor:unlock', '#', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (2000, '基础数据', 'TenantBasedate', 0, 5, '#', 'menuItem', 'M', '0', '', 'fa fa-group', 'admin', '2019-08-02 15:15:59', 'admin', '2019-08-16 13:43:08', '');
INSERT INTO `sys_menu` VALUES (2001, '消息中心', 'TenantMessage', 0, 6, '#', 'menuItem', 'M', '0', '', 'fa fa-envelope', 'admin', '2019-08-02 15:16:47', 'admin', '2019-08-07 10:59:17', '');
INSERT INTO `sys_menu` VALUES (2002, '地区管理', 'TenantBasedate', 2000, 8, '/basedata/districts', 'menuItem', 'C', '0', 'basedata:districts:view', 'fa fa-map-o', 'admin', '2019-08-16 09:13:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2003, '地区查询', 'TenantBasedate', 2002, 1, '#', 'menuItem', 'F', '0', 'basedata:districts:list', '#', 'admin', '2019-08-16 09:14:48', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2004, '导出', 'TenantBasedate', 2002, 2, '#', 'menuItem', 'F', '0', 'basedata:districts:export', '#', 'admin', '2019-08-16 09:15:34', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2005, '添加', 'TenantBasedate', 2002, 3, '#', 'menuItem', 'F', '0', 'basedata:districts:add', '#', 'admin', '2019-08-16 09:16:11', 'admin', '2019-08-16 09:17:33', '');
INSERT INTO `sys_menu` VALUES (2006, '编辑', 'TenantBasedate', 2002, 4, '#', 'menuItem', 'F', '0', 'basedata:districts:edit', '#', 'admin', '2019-08-16 09:16:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2007, '删除', 'TenantBasedate', 2002, 5, '#', 'menuItem', 'F', '0', 'basedata:districts:remove', '#', 'admin', '2019-08-16 09:17:21', '', NULL, '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '通知公告表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (1, '温馨提醒：2018-07-01 石头新版本发布啦', '2', '新版本内容', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2019-11-25 08:52:38', '管理员');
INSERT INTO `sys_notice` VALUES (2, '维护通知：2018-07-01 石头系统凌晨维护', '1', '维护内容', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '管理员');
INSERT INTO `sys_notice` VALUES (11, '通知各商家', '1', '<p>东奔西走</p>', '0', 'admin', '2019-11-26 15:31:09', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '返回参数',
  `status` int(1) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 534 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '操作日志记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位名称',
  `tenant_code` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '所属租户',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '岗位信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ceo', '董事长', '000000', 1, '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_post` VALUES (2, 'se', '项目经理', '000000', 2, '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_post` VALUES (3, 'hr', '人力资源', '000000', 3, '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '');
INSERT INTO `sys_post` VALUES (8, 'product', '产品', '111111', 1, '0', '111111_admin', '2019-08-16 15:46:47', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` int(16) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `tenant_code` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '000000' COMMENT '所属租户',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 124 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '管理员', 'admin', 1, '1', '000000', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '管理员');
INSERT INTO `sys_role` VALUES (2, '普通角色', 'common', 2, '2', '000000', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2019-08-02 15:13:22', '普通角色');
INSERT INTO `sys_role` VALUES (100, '昆仑科技_管理员', '222222_admin', 1, '1', '222222', '0', '0', 'admin', '2019-08-05 10:45:28', 'admin', '2019-08-14 11:03:54', '昆仑科技_管理员');
INSERT INTO `sys_role` VALUES (102, '振康物流_管理员', 'SDFSDFSDF', 1, '3', '111111', '0', '0', 'admin', '2019-08-06 18:23:53', 'admin', '2019-12-07 16:31:32', '振康物流_管理员');
INSERT INTO `sys_role` VALUES (104, '开发', 'develop', 2, '1', '111111', '0', '0', '111111_admin', '2019-08-14 11:46:23', '', NULL, '2');
INSERT INTO `sys_role` VALUES (108, '666', '6666', 666, '1', '111111', '0', '0', '111111_admin', '2019-08-14 14:14:28', 'test', '2019-10-24 15:50:14', '666');
INSERT INTO `sys_role` VALUES (112, 't1', 't1', 1, '4', '111111', '0', '0', 'test', '2019-10-24 15:45:31', 'test', '2019-10-24 16:32:08', NULL);
INSERT INTO `sys_role` VALUES (118, '租户试验平台_管理员', 'uDLBWt_admin', 1, '1', 'uDLBWt', '0', '0', 'admin', '2019-11-25 11:35:32', '', NULL, '租户试验平台_管理员');
INSERT INTO `sys_role` VALUES (119, '测试员', 'test', 0, '5', '000000', '0', '0', 'admin', '2019-12-03 18:50:00', 'admin', '2019-12-03 18:50:24', '1');
INSERT INTO `sys_role` VALUES (120, '厦门新星科技_管理员', 'xtvXeH_admin', 1, '1', 'xtvXeH', '0', '0', 'admin', '2019-12-06 17:01:01', '', NULL, '厦门新星科技_管理员');
INSERT INTO `sys_role` VALUES (121, '1', '1', 1, '3', '000000', '0', '0', 'admin', '2019-12-07 16:27:31', 'admin', '2019-12-07 16:27:42', '1');
INSERT INTO `sys_role` VALUES (122, 'rer', 'rr', 3, '1', '000000', '0', '0', 'admin', '2019-12-26 09:09:29', '', NULL, 'erre');
INSERT INTO `sys_role` VALUES (123, 'AIBDGO_管理员', 'ElRetC_admin', 1, '1', 'ElRetC', '0', '0', 'admin', '2019-12-28 17:47:11', '', NULL, 'AIBDGO_管理员');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` int(16) NOT NULL COMMENT '角色ID',
  `dept_id` int(16) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (2, 100);
INSERT INTO `sys_role_dept` VALUES (2, 101);
INSERT INTO `sys_role_dept` VALUES (2, 105);
INSERT INTO `sys_role_dept` VALUES (100, 200);
INSERT INTO `sys_role_dept` VALUES (118, 212);
INSERT INTO `sys_role_dept` VALUES (120, 216);
INSERT INTO `sys_role_dept` VALUES (123, 219);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` int(16) NOT NULL COMMENT '角色ID',
  `menu_id` int(16) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 2);
INSERT INTO `sys_role_menu` VALUES (2, 3);
INSERT INTO `sys_role_menu` VALUES (2, 100);
INSERT INTO `sys_role_menu` VALUES (2, 101);
INSERT INTO `sys_role_menu` VALUES (2, 102);
INSERT INTO `sys_role_menu` VALUES (2, 103);
INSERT INTO `sys_role_menu` VALUES (2, 104);
INSERT INTO `sys_role_menu` VALUES (2, 105);
INSERT INTO `sys_role_menu` VALUES (2, 106);
INSERT INTO `sys_role_menu` VALUES (2, 107);
INSERT INTO `sys_role_menu` VALUES (2, 108);
INSERT INTO `sys_role_menu` VALUES (2, 109);
INSERT INTO `sys_role_menu` VALUES (2, 110);
INSERT INTO `sys_role_menu` VALUES (2, 111);
INSERT INTO `sys_role_menu` VALUES (2, 112);
INSERT INTO `sys_role_menu` VALUES (2, 113);
INSERT INTO `sys_role_menu` VALUES (2, 114);
INSERT INTO `sys_role_menu` VALUES (2, 115);
INSERT INTO `sys_role_menu` VALUES (2, 500);
INSERT INTO `sys_role_menu` VALUES (2, 501);
INSERT INTO `sys_role_menu` VALUES (2, 1000);
INSERT INTO `sys_role_menu` VALUES (2, 1001);
INSERT INTO `sys_role_menu` VALUES (2, 1002);
INSERT INTO `sys_role_menu` VALUES (2, 1003);
INSERT INTO `sys_role_menu` VALUES (2, 1004);
INSERT INTO `sys_role_menu` VALUES (2, 1005);
INSERT INTO `sys_role_menu` VALUES (2, 1006);
INSERT INTO `sys_role_menu` VALUES (2, 1007);
INSERT INTO `sys_role_menu` VALUES (2, 1008);
INSERT INTO `sys_role_menu` VALUES (2, 1009);
INSERT INTO `sys_role_menu` VALUES (2, 1010);
INSERT INTO `sys_role_menu` VALUES (2, 1011);
INSERT INTO `sys_role_menu` VALUES (2, 1012);
INSERT INTO `sys_role_menu` VALUES (2, 1013);
INSERT INTO `sys_role_menu` VALUES (2, 1014);
INSERT INTO `sys_role_menu` VALUES (2, 1015);
INSERT INTO `sys_role_menu` VALUES (2, 1016);
INSERT INTO `sys_role_menu` VALUES (2, 1017);
INSERT INTO `sys_role_menu` VALUES (2, 1018);
INSERT INTO `sys_role_menu` VALUES (2, 1019);
INSERT INTO `sys_role_menu` VALUES (2, 1020);
INSERT INTO `sys_role_menu` VALUES (2, 1021);
INSERT INTO `sys_role_menu` VALUES (2, 1022);
INSERT INTO `sys_role_menu` VALUES (2, 1023);
INSERT INTO `sys_role_menu` VALUES (2, 1024);
INSERT INTO `sys_role_menu` VALUES (2, 1025);
INSERT INTO `sys_role_menu` VALUES (2, 1026);
INSERT INTO `sys_role_menu` VALUES (2, 1027);
INSERT INTO `sys_role_menu` VALUES (2, 1028);
INSERT INTO `sys_role_menu` VALUES (2, 1029);
INSERT INTO `sys_role_menu` VALUES (2, 1030);
INSERT INTO `sys_role_menu` VALUES (2, 1031);
INSERT INTO `sys_role_menu` VALUES (2, 1032);
INSERT INTO `sys_role_menu` VALUES (2, 1033);
INSERT INTO `sys_role_menu` VALUES (2, 1034);
INSERT INTO `sys_role_menu` VALUES (2, 1035);
INSERT INTO `sys_role_menu` VALUES (2, 1036);
INSERT INTO `sys_role_menu` VALUES (2, 1037);
INSERT INTO `sys_role_menu` VALUES (2, 1038);
INSERT INTO `sys_role_menu` VALUES (2, 1039);
INSERT INTO `sys_role_menu` VALUES (2, 1040);
INSERT INTO `sys_role_menu` VALUES (2, 1041);
INSERT INTO `sys_role_menu` VALUES (2, 1042);
INSERT INTO `sys_role_menu` VALUES (2, 1043);
INSERT INTO `sys_role_menu` VALUES (2, 1044);
INSERT INTO `sys_role_menu` VALUES (2, 1045);
INSERT INTO `sys_role_menu` VALUES (2, 1046);
INSERT INTO `sys_role_menu` VALUES (2, 1047);
INSERT INTO `sys_role_menu` VALUES (2, 1048);
INSERT INTO `sys_role_menu` VALUES (2, 1049);
INSERT INTO `sys_role_menu` VALUES (2, 1050);
INSERT INTO `sys_role_menu` VALUES (2, 1051);
INSERT INTO `sys_role_menu` VALUES (2, 1052);
INSERT INTO `sys_role_menu` VALUES (2, 1053);
INSERT INTO `sys_role_menu` VALUES (2, 1054);
INSERT INTO `sys_role_menu` VALUES (2, 1055);
INSERT INTO `sys_role_menu` VALUES (2, 1056);
INSERT INTO `sys_role_menu` VALUES (2, 1057);
INSERT INTO `sys_role_menu` VALUES (2, 1058);
INSERT INTO `sys_role_menu` VALUES (2, 1059);
INSERT INTO `sys_role_menu` VALUES (2, 1060);
INSERT INTO `sys_role_menu` VALUES (100, 3);
INSERT INTO `sys_role_menu` VALUES (100, 5);
INSERT INTO `sys_role_menu` VALUES (100, 100);
INSERT INTO `sys_role_menu` VALUES (100, 101);
INSERT INTO `sys_role_menu` VALUES (100, 102);
INSERT INTO `sys_role_menu` VALUES (100, 103);
INSERT INTO `sys_role_menu` VALUES (100, 104);
INSERT INTO `sys_role_menu` VALUES (100, 107);
INSERT INTO `sys_role_menu` VALUES (100, 115);
INSERT INTO `sys_role_menu` VALUES (100, 500);
INSERT INTO `sys_role_menu` VALUES (100, 501);
INSERT INTO `sys_role_menu` VALUES (100, 1000);
INSERT INTO `sys_role_menu` VALUES (100, 1001);
INSERT INTO `sys_role_menu` VALUES (100, 1002);
INSERT INTO `sys_role_menu` VALUES (100, 1003);
INSERT INTO `sys_role_menu` VALUES (100, 1004);
INSERT INTO `sys_role_menu` VALUES (100, 1005);
INSERT INTO `sys_role_menu` VALUES (100, 1006);
INSERT INTO `sys_role_menu` VALUES (100, 1007);
INSERT INTO `sys_role_menu` VALUES (100, 1008);
INSERT INTO `sys_role_menu` VALUES (100, 1009);
INSERT INTO `sys_role_menu` VALUES (100, 1010);
INSERT INTO `sys_role_menu` VALUES (100, 1011);
INSERT INTO `sys_role_menu` VALUES (100, 1012);
INSERT INTO `sys_role_menu` VALUES (100, 1013);
INSERT INTO `sys_role_menu` VALUES (100, 1016);
INSERT INTO `sys_role_menu` VALUES (100, 1017);
INSERT INTO `sys_role_menu` VALUES (100, 1018);
INSERT INTO `sys_role_menu` VALUES (100, 1019);
INSERT INTO `sys_role_menu` VALUES (100, 1020);
INSERT INTO `sys_role_menu` VALUES (100, 1021);
INSERT INTO `sys_role_menu` VALUES (100, 1022);
INSERT INTO `sys_role_menu` VALUES (100, 1023);
INSERT INTO `sys_role_menu` VALUES (100, 1024);
INSERT INTO `sys_role_menu` VALUES (100, 1035);
INSERT INTO `sys_role_menu` VALUES (100, 1036);
INSERT INTO `sys_role_menu` VALUES (100, 1037);
INSERT INTO `sys_role_menu` VALUES (100, 1038);
INSERT INTO `sys_role_menu` VALUES (100, 1039);
INSERT INTO `sys_role_menu` VALUES (100, 1040);
INSERT INTO `sys_role_menu` VALUES (100, 1041);
INSERT INTO `sys_role_menu` VALUES (100, 1042);
INSERT INTO `sys_role_menu` VALUES (100, 1043);
INSERT INTO `sys_role_menu` VALUES (100, 1044);
INSERT INTO `sys_role_menu` VALUES (100, 1045);
INSERT INTO `sys_role_menu` VALUES (100, 1062);
INSERT INTO `sys_role_menu` VALUES (100, 2000);
INSERT INTO `sys_role_menu` VALUES (100, 2001);
INSERT INTO `sys_role_menu` VALUES (102, 3);
INSERT INTO `sys_role_menu` VALUES (102, 5);
INSERT INTO `sys_role_menu` VALUES (102, 107);
INSERT INTO `sys_role_menu` VALUES (102, 115);
INSERT INTO `sys_role_menu` VALUES (102, 500);
INSERT INTO `sys_role_menu` VALUES (102, 501);
INSERT INTO `sys_role_menu` VALUES (102, 1035);
INSERT INTO `sys_role_menu` VALUES (102, 1036);
INSERT INTO `sys_role_menu` VALUES (102, 1037);
INSERT INTO `sys_role_menu` VALUES (102, 1038);
INSERT INTO `sys_role_menu` VALUES (102, 1039);
INSERT INTO `sys_role_menu` VALUES (102, 1040);
INSERT INTO `sys_role_menu` VALUES (102, 1041);
INSERT INTO `sys_role_menu` VALUES (102, 1042);
INSERT INTO `sys_role_menu` VALUES (102, 1043);
INSERT INTO `sys_role_menu` VALUES (102, 1044);
INSERT INTO `sys_role_menu` VALUES (102, 1045);
INSERT INTO `sys_role_menu` VALUES (102, 1062);
INSERT INTO `sys_role_menu` VALUES (102, 2001);
INSERT INTO `sys_role_menu` VALUES (103, 3);
INSERT INTO `sys_role_menu` VALUES (103, 5);
INSERT INTO `sys_role_menu` VALUES (103, 100);
INSERT INTO `sys_role_menu` VALUES (103, 107);
INSERT INTO `sys_role_menu` VALUES (103, 115);
INSERT INTO `sys_role_menu` VALUES (103, 500);
INSERT INTO `sys_role_menu` VALUES (103, 501);
INSERT INTO `sys_role_menu` VALUES (103, 1000);
INSERT INTO `sys_role_menu` VALUES (103, 1001);
INSERT INTO `sys_role_menu` VALUES (103, 1002);
INSERT INTO `sys_role_menu` VALUES (103, 1003);
INSERT INTO `sys_role_menu` VALUES (103, 1004);
INSERT INTO `sys_role_menu` VALUES (103, 1005);
INSERT INTO `sys_role_menu` VALUES (103, 1006);
INSERT INTO `sys_role_menu` VALUES (103, 1035);
INSERT INTO `sys_role_menu` VALUES (103, 1036);
INSERT INTO `sys_role_menu` VALUES (103, 1037);
INSERT INTO `sys_role_menu` VALUES (103, 1038);
INSERT INTO `sys_role_menu` VALUES (103, 1039);
INSERT INTO `sys_role_menu` VALUES (103, 1040);
INSERT INTO `sys_role_menu` VALUES (103, 1041);
INSERT INTO `sys_role_menu` VALUES (103, 1042);
INSERT INTO `sys_role_menu` VALUES (103, 1043);
INSERT INTO `sys_role_menu` VALUES (103, 1044);
INSERT INTO `sys_role_menu` VALUES (103, 1045);
INSERT INTO `sys_role_menu` VALUES (103, 1062);
INSERT INTO `sys_role_menu` VALUES (103, 2000);
INSERT INTO `sys_role_menu` VALUES (103, 2001);
INSERT INTO `sys_role_menu` VALUES (104, 3);
INSERT INTO `sys_role_menu` VALUES (104, 5);
INSERT INTO `sys_role_menu` VALUES (104, 107);
INSERT INTO `sys_role_menu` VALUES (104, 115);
INSERT INTO `sys_role_menu` VALUES (104, 500);
INSERT INTO `sys_role_menu` VALUES (104, 501);
INSERT INTO `sys_role_menu` VALUES (104, 1035);
INSERT INTO `sys_role_menu` VALUES (104, 1036);
INSERT INTO `sys_role_menu` VALUES (104, 1037);
INSERT INTO `sys_role_menu` VALUES (104, 1038);
INSERT INTO `sys_role_menu` VALUES (104, 1039);
INSERT INTO `sys_role_menu` VALUES (104, 1040);
INSERT INTO `sys_role_menu` VALUES (104, 1041);
INSERT INTO `sys_role_menu` VALUES (104, 1042);
INSERT INTO `sys_role_menu` VALUES (104, 1043);
INSERT INTO `sys_role_menu` VALUES (104, 1044);
INSERT INTO `sys_role_menu` VALUES (104, 1045);
INSERT INTO `sys_role_menu` VALUES (104, 1062);
INSERT INTO `sys_role_menu` VALUES (104, 2001);
INSERT INTO `sys_role_menu` VALUES (105, 3);
INSERT INTO `sys_role_menu` VALUES (105, 5);
INSERT INTO `sys_role_menu` VALUES (105, 107);
INSERT INTO `sys_role_menu` VALUES (105, 115);
INSERT INTO `sys_role_menu` VALUES (105, 500);
INSERT INTO `sys_role_menu` VALUES (105, 501);
INSERT INTO `sys_role_menu` VALUES (105, 1035);
INSERT INTO `sys_role_menu` VALUES (105, 1036);
INSERT INTO `sys_role_menu` VALUES (105, 1037);
INSERT INTO `sys_role_menu` VALUES (105, 1038);
INSERT INTO `sys_role_menu` VALUES (105, 1039);
INSERT INTO `sys_role_menu` VALUES (105, 1040);
INSERT INTO `sys_role_menu` VALUES (105, 1041);
INSERT INTO `sys_role_menu` VALUES (105, 1042);
INSERT INTO `sys_role_menu` VALUES (105, 1043);
INSERT INTO `sys_role_menu` VALUES (105, 1044);
INSERT INTO `sys_role_menu` VALUES (105, 1045);
INSERT INTO `sys_role_menu` VALUES (105, 1062);
INSERT INTO `sys_role_menu` VALUES (105, 2001);
INSERT INTO `sys_role_menu` VALUES (106, 3);
INSERT INTO `sys_role_menu` VALUES (106, 5);
INSERT INTO `sys_role_menu` VALUES (106, 107);
INSERT INTO `sys_role_menu` VALUES (106, 115);
INSERT INTO `sys_role_menu` VALUES (106, 500);
INSERT INTO `sys_role_menu` VALUES (106, 501);
INSERT INTO `sys_role_menu` VALUES (106, 1035);
INSERT INTO `sys_role_menu` VALUES (106, 1036);
INSERT INTO `sys_role_menu` VALUES (106, 1037);
INSERT INTO `sys_role_menu` VALUES (106, 1038);
INSERT INTO `sys_role_menu` VALUES (106, 1039);
INSERT INTO `sys_role_menu` VALUES (106, 1040);
INSERT INTO `sys_role_menu` VALUES (106, 1041);
INSERT INTO `sys_role_menu` VALUES (106, 1042);
INSERT INTO `sys_role_menu` VALUES (106, 1043);
INSERT INTO `sys_role_menu` VALUES (106, 1044);
INSERT INTO `sys_role_menu` VALUES (106, 1045);
INSERT INTO `sys_role_menu` VALUES (106, 1062);
INSERT INTO `sys_role_menu` VALUES (106, 2001);
INSERT INTO `sys_role_menu` VALUES (107, 3);
INSERT INTO `sys_role_menu` VALUES (107, 5);
INSERT INTO `sys_role_menu` VALUES (107, 107);
INSERT INTO `sys_role_menu` VALUES (107, 115);
INSERT INTO `sys_role_menu` VALUES (107, 500);
INSERT INTO `sys_role_menu` VALUES (107, 501);
INSERT INTO `sys_role_menu` VALUES (107, 1035);
INSERT INTO `sys_role_menu` VALUES (107, 1036);
INSERT INTO `sys_role_menu` VALUES (107, 1037);
INSERT INTO `sys_role_menu` VALUES (107, 1038);
INSERT INTO `sys_role_menu` VALUES (107, 1039);
INSERT INTO `sys_role_menu` VALUES (107, 1040);
INSERT INTO `sys_role_menu` VALUES (107, 1041);
INSERT INTO `sys_role_menu` VALUES (107, 1042);
INSERT INTO `sys_role_menu` VALUES (107, 1043);
INSERT INTO `sys_role_menu` VALUES (107, 1044);
INSERT INTO `sys_role_menu` VALUES (107, 1045);
INSERT INTO `sys_role_menu` VALUES (107, 1062);
INSERT INTO `sys_role_menu` VALUES (107, 2001);
INSERT INTO `sys_role_menu` VALUES (108, 5);
INSERT INTO `sys_role_menu` VALUES (108, 107);
INSERT INTO `sys_role_menu` VALUES (108, 500);
INSERT INTO `sys_role_menu` VALUES (108, 501);
INSERT INTO `sys_role_menu` VALUES (108, 1035);
INSERT INTO `sys_role_menu` VALUES (108, 1036);
INSERT INTO `sys_role_menu` VALUES (108, 1037);
INSERT INTO `sys_role_menu` VALUES (108, 1038);
INSERT INTO `sys_role_menu` VALUES (108, 1039);
INSERT INTO `sys_role_menu` VALUES (108, 1040);
INSERT INTO `sys_role_menu` VALUES (108, 1041);
INSERT INTO `sys_role_menu` VALUES (108, 1042);
INSERT INTO `sys_role_menu` VALUES (108, 1043);
INSERT INTO `sys_role_menu` VALUES (108, 1044);
INSERT INTO `sys_role_menu` VALUES (108, 1045);
INSERT INTO `sys_role_menu` VALUES (108, 1062);
INSERT INTO `sys_role_menu` VALUES (108, 2001);
INSERT INTO `sys_role_menu` VALUES (109, 3);
INSERT INTO `sys_role_menu` VALUES (109, 5);
INSERT INTO `sys_role_menu` VALUES (109, 100);
INSERT INTO `sys_role_menu` VALUES (109, 101);
INSERT INTO `sys_role_menu` VALUES (109, 102);
INSERT INTO `sys_role_menu` VALUES (109, 103);
INSERT INTO `sys_role_menu` VALUES (109, 104);
INSERT INTO `sys_role_menu` VALUES (109, 105);
INSERT INTO `sys_role_menu` VALUES (109, 106);
INSERT INTO `sys_role_menu` VALUES (109, 107);
INSERT INTO `sys_role_menu` VALUES (109, 115);
INSERT INTO `sys_role_menu` VALUES (109, 500);
INSERT INTO `sys_role_menu` VALUES (109, 501);
INSERT INTO `sys_role_menu` VALUES (109, 1000);
INSERT INTO `sys_role_menu` VALUES (109, 1001);
INSERT INTO `sys_role_menu` VALUES (109, 1002);
INSERT INTO `sys_role_menu` VALUES (109, 1003);
INSERT INTO `sys_role_menu` VALUES (109, 1004);
INSERT INTO `sys_role_menu` VALUES (109, 1005);
INSERT INTO `sys_role_menu` VALUES (109, 1006);
INSERT INTO `sys_role_menu` VALUES (109, 1007);
INSERT INTO `sys_role_menu` VALUES (109, 1008);
INSERT INTO `sys_role_menu` VALUES (109, 1009);
INSERT INTO `sys_role_menu` VALUES (109, 1010);
INSERT INTO `sys_role_menu` VALUES (109, 1011);
INSERT INTO `sys_role_menu` VALUES (109, 1012);
INSERT INTO `sys_role_menu` VALUES (109, 1013);
INSERT INTO `sys_role_menu` VALUES (109, 1014);
INSERT INTO `sys_role_menu` VALUES (109, 1015);
INSERT INTO `sys_role_menu` VALUES (109, 1016);
INSERT INTO `sys_role_menu` VALUES (109, 1017);
INSERT INTO `sys_role_menu` VALUES (109, 1018);
INSERT INTO `sys_role_menu` VALUES (109, 1019);
INSERT INTO `sys_role_menu` VALUES (109, 1020);
INSERT INTO `sys_role_menu` VALUES (109, 1021);
INSERT INTO `sys_role_menu` VALUES (109, 1022);
INSERT INTO `sys_role_menu` VALUES (109, 1023);
INSERT INTO `sys_role_menu` VALUES (109, 1024);
INSERT INTO `sys_role_menu` VALUES (109, 1025);
INSERT INTO `sys_role_menu` VALUES (109, 1026);
INSERT INTO `sys_role_menu` VALUES (109, 1027);
INSERT INTO `sys_role_menu` VALUES (109, 1028);
INSERT INTO `sys_role_menu` VALUES (109, 1029);
INSERT INTO `sys_role_menu` VALUES (109, 1030);
INSERT INTO `sys_role_menu` VALUES (109, 1031);
INSERT INTO `sys_role_menu` VALUES (109, 1032);
INSERT INTO `sys_role_menu` VALUES (109, 1033);
INSERT INTO `sys_role_menu` VALUES (109, 1034);
INSERT INTO `sys_role_menu` VALUES (109, 1035);
INSERT INTO `sys_role_menu` VALUES (109, 1036);
INSERT INTO `sys_role_menu` VALUES (109, 1037);
INSERT INTO `sys_role_menu` VALUES (109, 1038);
INSERT INTO `sys_role_menu` VALUES (109, 1039);
INSERT INTO `sys_role_menu` VALUES (109, 1040);
INSERT INTO `sys_role_menu` VALUES (109, 1041);
INSERT INTO `sys_role_menu` VALUES (109, 1042);
INSERT INTO `sys_role_menu` VALUES (109, 1043);
INSERT INTO `sys_role_menu` VALUES (109, 1044);
INSERT INTO `sys_role_menu` VALUES (109, 1045);
INSERT INTO `sys_role_menu` VALUES (109, 1062);
INSERT INTO `sys_role_menu` VALUES (109, 2000);
INSERT INTO `sys_role_menu` VALUES (109, 2001);
INSERT INTO `sys_role_menu` VALUES (109, 2002);
INSERT INTO `sys_role_menu` VALUES (109, 2003);
INSERT INTO `sys_role_menu` VALUES (109, 2004);
INSERT INTO `sys_role_menu` VALUES (109, 2005);
INSERT INTO `sys_role_menu` VALUES (109, 2006);
INSERT INTO `sys_role_menu` VALUES (109, 2007);
INSERT INTO `sys_role_menu` VALUES (110, 3);
INSERT INTO `sys_role_menu` VALUES (110, 5);
INSERT INTO `sys_role_menu` VALUES (110, 100);
INSERT INTO `sys_role_menu` VALUES (110, 101);
INSERT INTO `sys_role_menu` VALUES (110, 102);
INSERT INTO `sys_role_menu` VALUES (110, 103);
INSERT INTO `sys_role_menu` VALUES (110, 104);
INSERT INTO `sys_role_menu` VALUES (110, 105);
INSERT INTO `sys_role_menu` VALUES (110, 106);
INSERT INTO `sys_role_menu` VALUES (110, 115);
INSERT INTO `sys_role_menu` VALUES (110, 500);
INSERT INTO `sys_role_menu` VALUES (110, 501);
INSERT INTO `sys_role_menu` VALUES (110, 1000);
INSERT INTO `sys_role_menu` VALUES (110, 1001);
INSERT INTO `sys_role_menu` VALUES (110, 1002);
INSERT INTO `sys_role_menu` VALUES (110, 1003);
INSERT INTO `sys_role_menu` VALUES (110, 1004);
INSERT INTO `sys_role_menu` VALUES (110, 1005);
INSERT INTO `sys_role_menu` VALUES (110, 1006);
INSERT INTO `sys_role_menu` VALUES (110, 1007);
INSERT INTO `sys_role_menu` VALUES (110, 1008);
INSERT INTO `sys_role_menu` VALUES (110, 1009);
INSERT INTO `sys_role_menu` VALUES (110, 1010);
INSERT INTO `sys_role_menu` VALUES (110, 1011);
INSERT INTO `sys_role_menu` VALUES (110, 1012);
INSERT INTO `sys_role_menu` VALUES (110, 1013);
INSERT INTO `sys_role_menu` VALUES (110, 1014);
INSERT INTO `sys_role_menu` VALUES (110, 1015);
INSERT INTO `sys_role_menu` VALUES (110, 1016);
INSERT INTO `sys_role_menu` VALUES (110, 1017);
INSERT INTO `sys_role_menu` VALUES (110, 1018);
INSERT INTO `sys_role_menu` VALUES (110, 1019);
INSERT INTO `sys_role_menu` VALUES (110, 1020);
INSERT INTO `sys_role_menu` VALUES (110, 1021);
INSERT INTO `sys_role_menu` VALUES (110, 1022);
INSERT INTO `sys_role_menu` VALUES (110, 1023);
INSERT INTO `sys_role_menu` VALUES (110, 1024);
INSERT INTO `sys_role_menu` VALUES (110, 1025);
INSERT INTO `sys_role_menu` VALUES (110, 1026);
INSERT INTO `sys_role_menu` VALUES (110, 1027);
INSERT INTO `sys_role_menu` VALUES (110, 1028);
INSERT INTO `sys_role_menu` VALUES (110, 1029);
INSERT INTO `sys_role_menu` VALUES (110, 1030);
INSERT INTO `sys_role_menu` VALUES (110, 1031);
INSERT INTO `sys_role_menu` VALUES (110, 1032);
INSERT INTO `sys_role_menu` VALUES (110, 1033);
INSERT INTO `sys_role_menu` VALUES (110, 1034);
INSERT INTO `sys_role_menu` VALUES (110, 1039);
INSERT INTO `sys_role_menu` VALUES (110, 1040);
INSERT INTO `sys_role_menu` VALUES (110, 1041);
INSERT INTO `sys_role_menu` VALUES (110, 1042);
INSERT INTO `sys_role_menu` VALUES (110, 1043);
INSERT INTO `sys_role_menu` VALUES (110, 1044);
INSERT INTO `sys_role_menu` VALUES (110, 1045);
INSERT INTO `sys_role_menu` VALUES (110, 1062);
INSERT INTO `sys_role_menu` VALUES (110, 2000);
INSERT INTO `sys_role_menu` VALUES (110, 2002);
INSERT INTO `sys_role_menu` VALUES (110, 2003);
INSERT INTO `sys_role_menu` VALUES (110, 2004);
INSERT INTO `sys_role_menu` VALUES (110, 2005);
INSERT INTO `sys_role_menu` VALUES (110, 2006);
INSERT INTO `sys_role_menu` VALUES (110, 2007);
INSERT INTO `sys_role_menu` VALUES (111, 100);
INSERT INTO `sys_role_menu` VALUES (111, 101);
INSERT INTO `sys_role_menu` VALUES (111, 102);
INSERT INTO `sys_role_menu` VALUES (111, 103);
INSERT INTO `sys_role_menu` VALUES (111, 104);
INSERT INTO `sys_role_menu` VALUES (111, 105);
INSERT INTO `sys_role_menu` VALUES (111, 106);
INSERT INTO `sys_role_menu` VALUES (111, 1000);
INSERT INTO `sys_role_menu` VALUES (111, 1001);
INSERT INTO `sys_role_menu` VALUES (111, 1002);
INSERT INTO `sys_role_menu` VALUES (111, 1003);
INSERT INTO `sys_role_menu` VALUES (111, 1004);
INSERT INTO `sys_role_menu` VALUES (111, 1005);
INSERT INTO `sys_role_menu` VALUES (111, 1006);
INSERT INTO `sys_role_menu` VALUES (111, 1007);
INSERT INTO `sys_role_menu` VALUES (111, 1008);
INSERT INTO `sys_role_menu` VALUES (111, 1009);
INSERT INTO `sys_role_menu` VALUES (111, 1010);
INSERT INTO `sys_role_menu` VALUES (111, 1011);
INSERT INTO `sys_role_menu` VALUES (111, 1012);
INSERT INTO `sys_role_menu` VALUES (111, 1013);
INSERT INTO `sys_role_menu` VALUES (111, 1014);
INSERT INTO `sys_role_menu` VALUES (111, 1015);
INSERT INTO `sys_role_menu` VALUES (111, 1016);
INSERT INTO `sys_role_menu` VALUES (111, 1017);
INSERT INTO `sys_role_menu` VALUES (111, 1018);
INSERT INTO `sys_role_menu` VALUES (111, 1019);
INSERT INTO `sys_role_menu` VALUES (111, 1020);
INSERT INTO `sys_role_menu` VALUES (111, 1021);
INSERT INTO `sys_role_menu` VALUES (111, 1022);
INSERT INTO `sys_role_menu` VALUES (111, 1023);
INSERT INTO `sys_role_menu` VALUES (111, 1024);
INSERT INTO `sys_role_menu` VALUES (111, 1025);
INSERT INTO `sys_role_menu` VALUES (111, 1026);
INSERT INTO `sys_role_menu` VALUES (111, 1027);
INSERT INTO `sys_role_menu` VALUES (111, 1028);
INSERT INTO `sys_role_menu` VALUES (111, 1029);
INSERT INTO `sys_role_menu` VALUES (111, 1030);
INSERT INTO `sys_role_menu` VALUES (111, 1031);
INSERT INTO `sys_role_menu` VALUES (111, 1032);
INSERT INTO `sys_role_menu` VALUES (111, 1033);
INSERT INTO `sys_role_menu` VALUES (111, 1034);
INSERT INTO `sys_role_menu` VALUES (111, 2000);
INSERT INTO `sys_role_menu` VALUES (111, 2002);
INSERT INTO `sys_role_menu` VALUES (111, 2003);
INSERT INTO `sys_role_menu` VALUES (111, 2004);
INSERT INTO `sys_role_menu` VALUES (111, 2005);
INSERT INTO `sys_role_menu` VALUES (111, 2006);
INSERT INTO `sys_role_menu` VALUES (111, 2007);
INSERT INTO `sys_role_menu` VALUES (112, 1);
INSERT INTO `sys_role_menu` VALUES (112, 5);
INSERT INTO `sys_role_menu` VALUES (112, 107);
INSERT INTO `sys_role_menu` VALUES (112, 500);
INSERT INTO `sys_role_menu` VALUES (112, 501);
INSERT INTO `sys_role_menu` VALUES (112, 1035);
INSERT INTO `sys_role_menu` VALUES (112, 1036);
INSERT INTO `sys_role_menu` VALUES (112, 1037);
INSERT INTO `sys_role_menu` VALUES (112, 1038);
INSERT INTO `sys_role_menu` VALUES (112, 1039);
INSERT INTO `sys_role_menu` VALUES (112, 1040);
INSERT INTO `sys_role_menu` VALUES (112, 1041);
INSERT INTO `sys_role_menu` VALUES (112, 1042);
INSERT INTO `sys_role_menu` VALUES (112, 1043);
INSERT INTO `sys_role_menu` VALUES (112, 1044);
INSERT INTO `sys_role_menu` VALUES (112, 1045);
INSERT INTO `sys_role_menu` VALUES (112, 1059);
INSERT INTO `sys_role_menu` VALUES (112, 1060);
INSERT INTO `sys_role_menu` VALUES (112, 1062);
INSERT INTO `sys_role_menu` VALUES (112, 2001);
INSERT INTO `sys_role_menu` VALUES (113, 1);
INSERT INTO `sys_role_menu` VALUES (113, 5);
INSERT INTO `sys_role_menu` VALUES (113, 100);
INSERT INTO `sys_role_menu` VALUES (113, 107);
INSERT INTO `sys_role_menu` VALUES (113, 500);
INSERT INTO `sys_role_menu` VALUES (113, 501);
INSERT INTO `sys_role_menu` VALUES (113, 1000);
INSERT INTO `sys_role_menu` VALUES (113, 1001);
INSERT INTO `sys_role_menu` VALUES (113, 1002);
INSERT INTO `sys_role_menu` VALUES (113, 1003);
INSERT INTO `sys_role_menu` VALUES (113, 1004);
INSERT INTO `sys_role_menu` VALUES (113, 1005);
INSERT INTO `sys_role_menu` VALUES (113, 1006);
INSERT INTO `sys_role_menu` VALUES (113, 1035);
INSERT INTO `sys_role_menu` VALUES (113, 1036);
INSERT INTO `sys_role_menu` VALUES (113, 1037);
INSERT INTO `sys_role_menu` VALUES (113, 1038);
INSERT INTO `sys_role_menu` VALUES (113, 1039);
INSERT INTO `sys_role_menu` VALUES (113, 1040);
INSERT INTO `sys_role_menu` VALUES (113, 1041);
INSERT INTO `sys_role_menu` VALUES (113, 1042);
INSERT INTO `sys_role_menu` VALUES (113, 1043);
INSERT INTO `sys_role_menu` VALUES (113, 1044);
INSERT INTO `sys_role_menu` VALUES (113, 1045);
INSERT INTO `sys_role_menu` VALUES (113, 1059);
INSERT INTO `sys_role_menu` VALUES (113, 1060);
INSERT INTO `sys_role_menu` VALUES (113, 1062);
INSERT INTO `sys_role_menu` VALUES (113, 2000);
INSERT INTO `sys_role_menu` VALUES (113, 2001);
INSERT INTO `sys_role_menu` VALUES (114, 1);
INSERT INTO `sys_role_menu` VALUES (114, 5);
INSERT INTO `sys_role_menu` VALUES (114, 100);
INSERT INTO `sys_role_menu` VALUES (114, 101);
INSERT INTO `sys_role_menu` VALUES (114, 102);
INSERT INTO `sys_role_menu` VALUES (114, 103);
INSERT INTO `sys_role_menu` VALUES (114, 104);
INSERT INTO `sys_role_menu` VALUES (114, 107);
INSERT INTO `sys_role_menu` VALUES (114, 500);
INSERT INTO `sys_role_menu` VALUES (114, 501);
INSERT INTO `sys_role_menu` VALUES (114, 1000);
INSERT INTO `sys_role_menu` VALUES (114, 1001);
INSERT INTO `sys_role_menu` VALUES (114, 1002);
INSERT INTO `sys_role_menu` VALUES (114, 1003);
INSERT INTO `sys_role_menu` VALUES (114, 1004);
INSERT INTO `sys_role_menu` VALUES (114, 1005);
INSERT INTO `sys_role_menu` VALUES (114, 1006);
INSERT INTO `sys_role_menu` VALUES (114, 1007);
INSERT INTO `sys_role_menu` VALUES (114, 1008);
INSERT INTO `sys_role_menu` VALUES (114, 1009);
INSERT INTO `sys_role_menu` VALUES (114, 1010);
INSERT INTO `sys_role_menu` VALUES (114, 1011);
INSERT INTO `sys_role_menu` VALUES (114, 1012);
INSERT INTO `sys_role_menu` VALUES (114, 1013);
INSERT INTO `sys_role_menu` VALUES (114, 1014);
INSERT INTO `sys_role_menu` VALUES (114, 1015);
INSERT INTO `sys_role_menu` VALUES (114, 1016);
INSERT INTO `sys_role_menu` VALUES (114, 1017);
INSERT INTO `sys_role_menu` VALUES (114, 1018);
INSERT INTO `sys_role_menu` VALUES (114, 1019);
INSERT INTO `sys_role_menu` VALUES (114, 1020);
INSERT INTO `sys_role_menu` VALUES (114, 1021);
INSERT INTO `sys_role_menu` VALUES (114, 1022);
INSERT INTO `sys_role_menu` VALUES (114, 1023);
INSERT INTO `sys_role_menu` VALUES (114, 1024);
INSERT INTO `sys_role_menu` VALUES (114, 1035);
INSERT INTO `sys_role_menu` VALUES (114, 1036);
INSERT INTO `sys_role_menu` VALUES (114, 1037);
INSERT INTO `sys_role_menu` VALUES (114, 1038);
INSERT INTO `sys_role_menu` VALUES (114, 1039);
INSERT INTO `sys_role_menu` VALUES (114, 1040);
INSERT INTO `sys_role_menu` VALUES (114, 1041);
INSERT INTO `sys_role_menu` VALUES (114, 1042);
INSERT INTO `sys_role_menu` VALUES (114, 1043);
INSERT INTO `sys_role_menu` VALUES (114, 1044);
INSERT INTO `sys_role_menu` VALUES (114, 1045);
INSERT INTO `sys_role_menu` VALUES (114, 1059);
INSERT INTO `sys_role_menu` VALUES (114, 1060);
INSERT INTO `sys_role_menu` VALUES (114, 1062);
INSERT INTO `sys_role_menu` VALUES (114, 2000);
INSERT INTO `sys_role_menu` VALUES (114, 2001);
INSERT INTO `sys_role_menu` VALUES (115, 1);
INSERT INTO `sys_role_menu` VALUES (115, 5);
INSERT INTO `sys_role_menu` VALUES (115, 100);
INSERT INTO `sys_role_menu` VALUES (115, 101);
INSERT INTO `sys_role_menu` VALUES (115, 102);
INSERT INTO `sys_role_menu` VALUES (115, 103);
INSERT INTO `sys_role_menu` VALUES (115, 104);
INSERT INTO `sys_role_menu` VALUES (115, 105);
INSERT INTO `sys_role_menu` VALUES (115, 106);
INSERT INTO `sys_role_menu` VALUES (115, 107);
INSERT INTO `sys_role_menu` VALUES (115, 500);
INSERT INTO `sys_role_menu` VALUES (115, 501);
INSERT INTO `sys_role_menu` VALUES (115, 1000);
INSERT INTO `sys_role_menu` VALUES (115, 1001);
INSERT INTO `sys_role_menu` VALUES (115, 1002);
INSERT INTO `sys_role_menu` VALUES (115, 1003);
INSERT INTO `sys_role_menu` VALUES (115, 1004);
INSERT INTO `sys_role_menu` VALUES (115, 1005);
INSERT INTO `sys_role_menu` VALUES (115, 1006);
INSERT INTO `sys_role_menu` VALUES (115, 1007);
INSERT INTO `sys_role_menu` VALUES (115, 1008);
INSERT INTO `sys_role_menu` VALUES (115, 1009);
INSERT INTO `sys_role_menu` VALUES (115, 1010);
INSERT INTO `sys_role_menu` VALUES (115, 1011);
INSERT INTO `sys_role_menu` VALUES (115, 1012);
INSERT INTO `sys_role_menu` VALUES (115, 1013);
INSERT INTO `sys_role_menu` VALUES (115, 1014);
INSERT INTO `sys_role_menu` VALUES (115, 1015);
INSERT INTO `sys_role_menu` VALUES (115, 1016);
INSERT INTO `sys_role_menu` VALUES (115, 1017);
INSERT INTO `sys_role_menu` VALUES (115, 1018);
INSERT INTO `sys_role_menu` VALUES (115, 1019);
INSERT INTO `sys_role_menu` VALUES (115, 1020);
INSERT INTO `sys_role_menu` VALUES (115, 1021);
INSERT INTO `sys_role_menu` VALUES (115, 1022);
INSERT INTO `sys_role_menu` VALUES (115, 1023);
INSERT INTO `sys_role_menu` VALUES (115, 1024);
INSERT INTO `sys_role_menu` VALUES (115, 1025);
INSERT INTO `sys_role_menu` VALUES (115, 1026);
INSERT INTO `sys_role_menu` VALUES (115, 1027);
INSERT INTO `sys_role_menu` VALUES (115, 1028);
INSERT INTO `sys_role_menu` VALUES (115, 1029);
INSERT INTO `sys_role_menu` VALUES (115, 1030);
INSERT INTO `sys_role_menu` VALUES (115, 1031);
INSERT INTO `sys_role_menu` VALUES (115, 1032);
INSERT INTO `sys_role_menu` VALUES (115, 1033);
INSERT INTO `sys_role_menu` VALUES (115, 1034);
INSERT INTO `sys_role_menu` VALUES (115, 1035);
INSERT INTO `sys_role_menu` VALUES (115, 1036);
INSERT INTO `sys_role_menu` VALUES (115, 1037);
INSERT INTO `sys_role_menu` VALUES (115, 1038);
INSERT INTO `sys_role_menu` VALUES (115, 1039);
INSERT INTO `sys_role_menu` VALUES (115, 1040);
INSERT INTO `sys_role_menu` VALUES (115, 1041);
INSERT INTO `sys_role_menu` VALUES (115, 1042);
INSERT INTO `sys_role_menu` VALUES (115, 1043);
INSERT INTO `sys_role_menu` VALUES (115, 1044);
INSERT INTO `sys_role_menu` VALUES (115, 1045);
INSERT INTO `sys_role_menu` VALUES (115, 1059);
INSERT INTO `sys_role_menu` VALUES (115, 1060);
INSERT INTO `sys_role_menu` VALUES (115, 1062);
INSERT INTO `sys_role_menu` VALUES (115, 2000);
INSERT INTO `sys_role_menu` VALUES (115, 2001);
INSERT INTO `sys_role_menu` VALUES (116, 1);
INSERT INTO `sys_role_menu` VALUES (116, 5);
INSERT INTO `sys_role_menu` VALUES (116, 100);
INSERT INTO `sys_role_menu` VALUES (116, 101);
INSERT INTO `sys_role_menu` VALUES (116, 102);
INSERT INTO `sys_role_menu` VALUES (116, 103);
INSERT INTO `sys_role_menu` VALUES (116, 104);
INSERT INTO `sys_role_menu` VALUES (116, 105);
INSERT INTO `sys_role_menu` VALUES (116, 106);
INSERT INTO `sys_role_menu` VALUES (116, 107);
INSERT INTO `sys_role_menu` VALUES (116, 500);
INSERT INTO `sys_role_menu` VALUES (116, 501);
INSERT INTO `sys_role_menu` VALUES (116, 1000);
INSERT INTO `sys_role_menu` VALUES (116, 1001);
INSERT INTO `sys_role_menu` VALUES (116, 1002);
INSERT INTO `sys_role_menu` VALUES (116, 1003);
INSERT INTO `sys_role_menu` VALUES (116, 1004);
INSERT INTO `sys_role_menu` VALUES (116, 1005);
INSERT INTO `sys_role_menu` VALUES (116, 1006);
INSERT INTO `sys_role_menu` VALUES (116, 1007);
INSERT INTO `sys_role_menu` VALUES (116, 1008);
INSERT INTO `sys_role_menu` VALUES (116, 1009);
INSERT INTO `sys_role_menu` VALUES (116, 1010);
INSERT INTO `sys_role_menu` VALUES (116, 1011);
INSERT INTO `sys_role_menu` VALUES (116, 1012);
INSERT INTO `sys_role_menu` VALUES (116, 1013);
INSERT INTO `sys_role_menu` VALUES (116, 1014);
INSERT INTO `sys_role_menu` VALUES (116, 1015);
INSERT INTO `sys_role_menu` VALUES (116, 1016);
INSERT INTO `sys_role_menu` VALUES (116, 1017);
INSERT INTO `sys_role_menu` VALUES (116, 1018);
INSERT INTO `sys_role_menu` VALUES (116, 1019);
INSERT INTO `sys_role_menu` VALUES (116, 1020);
INSERT INTO `sys_role_menu` VALUES (116, 1021);
INSERT INTO `sys_role_menu` VALUES (116, 1022);
INSERT INTO `sys_role_menu` VALUES (116, 1023);
INSERT INTO `sys_role_menu` VALUES (116, 1024);
INSERT INTO `sys_role_menu` VALUES (116, 1025);
INSERT INTO `sys_role_menu` VALUES (116, 1026);
INSERT INTO `sys_role_menu` VALUES (116, 1027);
INSERT INTO `sys_role_menu` VALUES (116, 1028);
INSERT INTO `sys_role_menu` VALUES (116, 1029);
INSERT INTO `sys_role_menu` VALUES (116, 1030);
INSERT INTO `sys_role_menu` VALUES (116, 1031);
INSERT INTO `sys_role_menu` VALUES (116, 1032);
INSERT INTO `sys_role_menu` VALUES (116, 1033);
INSERT INTO `sys_role_menu` VALUES (116, 1034);
INSERT INTO `sys_role_menu` VALUES (116, 1035);
INSERT INTO `sys_role_menu` VALUES (116, 1036);
INSERT INTO `sys_role_menu` VALUES (116, 1037);
INSERT INTO `sys_role_menu` VALUES (116, 1038);
INSERT INTO `sys_role_menu` VALUES (116, 1039);
INSERT INTO `sys_role_menu` VALUES (116, 1040);
INSERT INTO `sys_role_menu` VALUES (116, 1041);
INSERT INTO `sys_role_menu` VALUES (116, 1042);
INSERT INTO `sys_role_menu` VALUES (116, 1043);
INSERT INTO `sys_role_menu` VALUES (116, 1044);
INSERT INTO `sys_role_menu` VALUES (116, 1045);
INSERT INTO `sys_role_menu` VALUES (116, 1059);
INSERT INTO `sys_role_menu` VALUES (116, 1060);
INSERT INTO `sys_role_menu` VALUES (116, 1062);
INSERT INTO `sys_role_menu` VALUES (116, 2000);
INSERT INTO `sys_role_menu` VALUES (116, 2001);
INSERT INTO `sys_role_menu` VALUES (117, 100);
INSERT INTO `sys_role_menu` VALUES (117, 101);
INSERT INTO `sys_role_menu` VALUES (117, 102);
INSERT INTO `sys_role_menu` VALUES (117, 103);
INSERT INTO `sys_role_menu` VALUES (117, 104);
INSERT INTO `sys_role_menu` VALUES (117, 105);
INSERT INTO `sys_role_menu` VALUES (117, 106);
INSERT INTO `sys_role_menu` VALUES (117, 1000);
INSERT INTO `sys_role_menu` VALUES (117, 1001);
INSERT INTO `sys_role_menu` VALUES (117, 1002);
INSERT INTO `sys_role_menu` VALUES (117, 1003);
INSERT INTO `sys_role_menu` VALUES (117, 1004);
INSERT INTO `sys_role_menu` VALUES (117, 1005);
INSERT INTO `sys_role_menu` VALUES (117, 1006);
INSERT INTO `sys_role_menu` VALUES (117, 1007);
INSERT INTO `sys_role_menu` VALUES (117, 1008);
INSERT INTO `sys_role_menu` VALUES (117, 1009);
INSERT INTO `sys_role_menu` VALUES (117, 1010);
INSERT INTO `sys_role_menu` VALUES (117, 1011);
INSERT INTO `sys_role_menu` VALUES (117, 1012);
INSERT INTO `sys_role_menu` VALUES (117, 1013);
INSERT INTO `sys_role_menu` VALUES (117, 1014);
INSERT INTO `sys_role_menu` VALUES (117, 1015);
INSERT INTO `sys_role_menu` VALUES (117, 1016);
INSERT INTO `sys_role_menu` VALUES (117, 1017);
INSERT INTO `sys_role_menu` VALUES (117, 1018);
INSERT INTO `sys_role_menu` VALUES (117, 1019);
INSERT INTO `sys_role_menu` VALUES (117, 1020);
INSERT INTO `sys_role_menu` VALUES (117, 1021);
INSERT INTO `sys_role_menu` VALUES (117, 1022);
INSERT INTO `sys_role_menu` VALUES (117, 1023);
INSERT INTO `sys_role_menu` VALUES (117, 1024);
INSERT INTO `sys_role_menu` VALUES (117, 1025);
INSERT INTO `sys_role_menu` VALUES (117, 1026);
INSERT INTO `sys_role_menu` VALUES (117, 1027);
INSERT INTO `sys_role_menu` VALUES (117, 1028);
INSERT INTO `sys_role_menu` VALUES (117, 1029);
INSERT INTO `sys_role_menu` VALUES (117, 1030);
INSERT INTO `sys_role_menu` VALUES (117, 1031);
INSERT INTO `sys_role_menu` VALUES (117, 1032);
INSERT INTO `sys_role_menu` VALUES (117, 1033);
INSERT INTO `sys_role_menu` VALUES (117, 1034);
INSERT INTO `sys_role_menu` VALUES (117, 2000);
INSERT INTO `sys_role_menu` VALUES (117, 2002);
INSERT INTO `sys_role_menu` VALUES (117, 2003);
INSERT INTO `sys_role_menu` VALUES (117, 2004);
INSERT INTO `sys_role_menu` VALUES (117, 2005);
INSERT INTO `sys_role_menu` VALUES (117, 2006);
INSERT INTO `sys_role_menu` VALUES (117, 2007);
INSERT INTO `sys_role_menu` VALUES (118, 5);
INSERT INTO `sys_role_menu` VALUES (118, 100);
INSERT INTO `sys_role_menu` VALUES (118, 101);
INSERT INTO `sys_role_menu` VALUES (118, 102);
INSERT INTO `sys_role_menu` VALUES (118, 103);
INSERT INTO `sys_role_menu` VALUES (118, 104);
INSERT INTO `sys_role_menu` VALUES (118, 105);
INSERT INTO `sys_role_menu` VALUES (118, 106);
INSERT INTO `sys_role_menu` VALUES (118, 500);
INSERT INTO `sys_role_menu` VALUES (118, 501);
INSERT INTO `sys_role_menu` VALUES (118, 1000);
INSERT INTO `sys_role_menu` VALUES (118, 1001);
INSERT INTO `sys_role_menu` VALUES (118, 1002);
INSERT INTO `sys_role_menu` VALUES (118, 1003);
INSERT INTO `sys_role_menu` VALUES (118, 1004);
INSERT INTO `sys_role_menu` VALUES (118, 1005);
INSERT INTO `sys_role_menu` VALUES (118, 1006);
INSERT INTO `sys_role_menu` VALUES (118, 1007);
INSERT INTO `sys_role_menu` VALUES (118, 1008);
INSERT INTO `sys_role_menu` VALUES (118, 1009);
INSERT INTO `sys_role_menu` VALUES (118, 1010);
INSERT INTO `sys_role_menu` VALUES (118, 1011);
INSERT INTO `sys_role_menu` VALUES (118, 1012);
INSERT INTO `sys_role_menu` VALUES (118, 1013);
INSERT INTO `sys_role_menu` VALUES (118, 1014);
INSERT INTO `sys_role_menu` VALUES (118, 1015);
INSERT INTO `sys_role_menu` VALUES (118, 1016);
INSERT INTO `sys_role_menu` VALUES (118, 1017);
INSERT INTO `sys_role_menu` VALUES (118, 1018);
INSERT INTO `sys_role_menu` VALUES (118, 1019);
INSERT INTO `sys_role_menu` VALUES (118, 1020);
INSERT INTO `sys_role_menu` VALUES (118, 1021);
INSERT INTO `sys_role_menu` VALUES (118, 1022);
INSERT INTO `sys_role_menu` VALUES (118, 1023);
INSERT INTO `sys_role_menu` VALUES (118, 1024);
INSERT INTO `sys_role_menu` VALUES (118, 1025);
INSERT INTO `sys_role_menu` VALUES (118, 1026);
INSERT INTO `sys_role_menu` VALUES (118, 1027);
INSERT INTO `sys_role_menu` VALUES (118, 1028);
INSERT INTO `sys_role_menu` VALUES (118, 1029);
INSERT INTO `sys_role_menu` VALUES (118, 1030);
INSERT INTO `sys_role_menu` VALUES (118, 1031);
INSERT INTO `sys_role_menu` VALUES (118, 1032);
INSERT INTO `sys_role_menu` VALUES (118, 1033);
INSERT INTO `sys_role_menu` VALUES (118, 1034);
INSERT INTO `sys_role_menu` VALUES (118, 1039);
INSERT INTO `sys_role_menu` VALUES (118, 1040);
INSERT INTO `sys_role_menu` VALUES (118, 1041);
INSERT INTO `sys_role_menu` VALUES (118, 1042);
INSERT INTO `sys_role_menu` VALUES (118, 1043);
INSERT INTO `sys_role_menu` VALUES (118, 1044);
INSERT INTO `sys_role_menu` VALUES (118, 1045);
INSERT INTO `sys_role_menu` VALUES (118, 1062);
INSERT INTO `sys_role_menu` VALUES (118, 2000);
INSERT INTO `sys_role_menu` VALUES (118, 2002);
INSERT INTO `sys_role_menu` VALUES (118, 2003);
INSERT INTO `sys_role_menu` VALUES (118, 2004);
INSERT INTO `sys_role_menu` VALUES (118, 2005);
INSERT INTO `sys_role_menu` VALUES (118, 2006);
INSERT INTO `sys_role_menu` VALUES (118, 2007);
INSERT INTO `sys_role_menu` VALUES (119, 100);
INSERT INTO `sys_role_menu` VALUES (119, 101);
INSERT INTO `sys_role_menu` VALUES (119, 102);
INSERT INTO `sys_role_menu` VALUES (119, 103);
INSERT INTO `sys_role_menu` VALUES (119, 104);
INSERT INTO `sys_role_menu` VALUES (119, 105);
INSERT INTO `sys_role_menu` VALUES (119, 106);
INSERT INTO `sys_role_menu` VALUES (119, 1000);
INSERT INTO `sys_role_menu` VALUES (119, 1001);
INSERT INTO `sys_role_menu` VALUES (119, 1002);
INSERT INTO `sys_role_menu` VALUES (119, 1003);
INSERT INTO `sys_role_menu` VALUES (119, 1004);
INSERT INTO `sys_role_menu` VALUES (119, 1005);
INSERT INTO `sys_role_menu` VALUES (119, 1006);
INSERT INTO `sys_role_menu` VALUES (119, 1007);
INSERT INTO `sys_role_menu` VALUES (119, 1008);
INSERT INTO `sys_role_menu` VALUES (119, 1009);
INSERT INTO `sys_role_menu` VALUES (119, 1010);
INSERT INTO `sys_role_menu` VALUES (119, 1011);
INSERT INTO `sys_role_menu` VALUES (119, 1012);
INSERT INTO `sys_role_menu` VALUES (119, 1013);
INSERT INTO `sys_role_menu` VALUES (119, 1014);
INSERT INTO `sys_role_menu` VALUES (119, 1015);
INSERT INTO `sys_role_menu` VALUES (119, 1016);
INSERT INTO `sys_role_menu` VALUES (119, 1017);
INSERT INTO `sys_role_menu` VALUES (119, 1018);
INSERT INTO `sys_role_menu` VALUES (119, 1019);
INSERT INTO `sys_role_menu` VALUES (119, 1020);
INSERT INTO `sys_role_menu` VALUES (119, 1021);
INSERT INTO `sys_role_menu` VALUES (119, 1022);
INSERT INTO `sys_role_menu` VALUES (119, 1023);
INSERT INTO `sys_role_menu` VALUES (119, 1024);
INSERT INTO `sys_role_menu` VALUES (119, 1025);
INSERT INTO `sys_role_menu` VALUES (119, 1026);
INSERT INTO `sys_role_menu` VALUES (119, 1027);
INSERT INTO `sys_role_menu` VALUES (119, 1028);
INSERT INTO `sys_role_menu` VALUES (119, 1029);
INSERT INTO `sys_role_menu` VALUES (119, 1030);
INSERT INTO `sys_role_menu` VALUES (119, 1031);
INSERT INTO `sys_role_menu` VALUES (119, 1032);
INSERT INTO `sys_role_menu` VALUES (119, 1033);
INSERT INTO `sys_role_menu` VALUES (119, 1034);
INSERT INTO `sys_role_menu` VALUES (119, 2000);
INSERT INTO `sys_role_menu` VALUES (119, 2002);
INSERT INTO `sys_role_menu` VALUES (119, 2003);
INSERT INTO `sys_role_menu` VALUES (119, 2004);
INSERT INTO `sys_role_menu` VALUES (119, 2005);
INSERT INTO `sys_role_menu` VALUES (119, 2006);
INSERT INTO `sys_role_menu` VALUES (119, 2007);
INSERT INTO `sys_role_menu` VALUES (120, 5);
INSERT INTO `sys_role_menu` VALUES (120, 100);
INSERT INTO `sys_role_menu` VALUES (120, 101);
INSERT INTO `sys_role_menu` VALUES (120, 102);
INSERT INTO `sys_role_menu` VALUES (120, 103);
INSERT INTO `sys_role_menu` VALUES (120, 104);
INSERT INTO `sys_role_menu` VALUES (120, 105);
INSERT INTO `sys_role_menu` VALUES (120, 106);
INSERT INTO `sys_role_menu` VALUES (120, 107);
INSERT INTO `sys_role_menu` VALUES (120, 500);
INSERT INTO `sys_role_menu` VALUES (120, 501);
INSERT INTO `sys_role_menu` VALUES (120, 1000);
INSERT INTO `sys_role_menu` VALUES (120, 1001);
INSERT INTO `sys_role_menu` VALUES (120, 1002);
INSERT INTO `sys_role_menu` VALUES (120, 1003);
INSERT INTO `sys_role_menu` VALUES (120, 1004);
INSERT INTO `sys_role_menu` VALUES (120, 1005);
INSERT INTO `sys_role_menu` VALUES (120, 1006);
INSERT INTO `sys_role_menu` VALUES (120, 1007);
INSERT INTO `sys_role_menu` VALUES (120, 1008);
INSERT INTO `sys_role_menu` VALUES (120, 1009);
INSERT INTO `sys_role_menu` VALUES (120, 1010);
INSERT INTO `sys_role_menu` VALUES (120, 1011);
INSERT INTO `sys_role_menu` VALUES (120, 1012);
INSERT INTO `sys_role_menu` VALUES (120, 1013);
INSERT INTO `sys_role_menu` VALUES (120, 1014);
INSERT INTO `sys_role_menu` VALUES (120, 1015);
INSERT INTO `sys_role_menu` VALUES (120, 1016);
INSERT INTO `sys_role_menu` VALUES (120, 1017);
INSERT INTO `sys_role_menu` VALUES (120, 1018);
INSERT INTO `sys_role_menu` VALUES (120, 1019);
INSERT INTO `sys_role_menu` VALUES (120, 1020);
INSERT INTO `sys_role_menu` VALUES (120, 1021);
INSERT INTO `sys_role_menu` VALUES (120, 1022);
INSERT INTO `sys_role_menu` VALUES (120, 1023);
INSERT INTO `sys_role_menu` VALUES (120, 1024);
INSERT INTO `sys_role_menu` VALUES (120, 1025);
INSERT INTO `sys_role_menu` VALUES (120, 1026);
INSERT INTO `sys_role_menu` VALUES (120, 1027);
INSERT INTO `sys_role_menu` VALUES (120, 1028);
INSERT INTO `sys_role_menu` VALUES (120, 1029);
INSERT INTO `sys_role_menu` VALUES (120, 1030);
INSERT INTO `sys_role_menu` VALUES (120, 1031);
INSERT INTO `sys_role_menu` VALUES (120, 1032);
INSERT INTO `sys_role_menu` VALUES (120, 1033);
INSERT INTO `sys_role_menu` VALUES (120, 1034);
INSERT INTO `sys_role_menu` VALUES (120, 1035);
INSERT INTO `sys_role_menu` VALUES (120, 1036);
INSERT INTO `sys_role_menu` VALUES (120, 1037);
INSERT INTO `sys_role_menu` VALUES (120, 1038);
INSERT INTO `sys_role_menu` VALUES (120, 1039);
INSERT INTO `sys_role_menu` VALUES (120, 1040);
INSERT INTO `sys_role_menu` VALUES (120, 1041);
INSERT INTO `sys_role_menu` VALUES (120, 1042);
INSERT INTO `sys_role_menu` VALUES (120, 1043);
INSERT INTO `sys_role_menu` VALUES (120, 1044);
INSERT INTO `sys_role_menu` VALUES (120, 1045);
INSERT INTO `sys_role_menu` VALUES (120, 1062);
INSERT INTO `sys_role_menu` VALUES (120, 2000);
INSERT INTO `sys_role_menu` VALUES (120, 2001);
INSERT INTO `sys_role_menu` VALUES (120, 2002);
INSERT INTO `sys_role_menu` VALUES (120, 2003);
INSERT INTO `sys_role_menu` VALUES (120, 2004);
INSERT INTO `sys_role_menu` VALUES (120, 2005);
INSERT INTO `sys_role_menu` VALUES (120, 2006);
INSERT INTO `sys_role_menu` VALUES (120, 2007);
INSERT INTO `sys_role_menu` VALUES (121, 1);
INSERT INTO `sys_role_menu` VALUES (121, 1060);
INSERT INTO `sys_role_menu` VALUES (122, 4);
INSERT INTO `sys_role_menu` VALUES (122, 110);
INSERT INTO `sys_role_menu` VALUES (122, 1049);
INSERT INTO `sys_role_menu` VALUES (122, 1050);
INSERT INTO `sys_role_menu` VALUES (122, 1051);
INSERT INTO `sys_role_menu` VALUES (122, 1052);
INSERT INTO `sys_role_menu` VALUES (122, 1053);
INSERT INTO `sys_role_menu` VALUES (122, 1054);
INSERT INTO `sys_role_menu` VALUES (122, 1055);
INSERT INTO `sys_role_menu` VALUES (123, 3);
INSERT INTO `sys_role_menu` VALUES (123, 100);
INSERT INTO `sys_role_menu` VALUES (123, 101);
INSERT INTO `sys_role_menu` VALUES (123, 102);
INSERT INTO `sys_role_menu` VALUES (123, 103);
INSERT INTO `sys_role_menu` VALUES (123, 104);
INSERT INTO `sys_role_menu` VALUES (123, 105);
INSERT INTO `sys_role_menu` VALUES (123, 106);
INSERT INTO `sys_role_menu` VALUES (123, 107);
INSERT INTO `sys_role_menu` VALUES (123, 115);
INSERT INTO `sys_role_menu` VALUES (123, 1000);
INSERT INTO `sys_role_menu` VALUES (123, 1001);
INSERT INTO `sys_role_menu` VALUES (123, 1002);
INSERT INTO `sys_role_menu` VALUES (123, 1003);
INSERT INTO `sys_role_menu` VALUES (123, 1004);
INSERT INTO `sys_role_menu` VALUES (123, 1005);
INSERT INTO `sys_role_menu` VALUES (123, 1006);
INSERT INTO `sys_role_menu` VALUES (123, 1007);
INSERT INTO `sys_role_menu` VALUES (123, 1008);
INSERT INTO `sys_role_menu` VALUES (123, 1009);
INSERT INTO `sys_role_menu` VALUES (123, 1010);
INSERT INTO `sys_role_menu` VALUES (123, 1011);
INSERT INTO `sys_role_menu` VALUES (123, 1012);
INSERT INTO `sys_role_menu` VALUES (123, 1013);
INSERT INTO `sys_role_menu` VALUES (123, 1014);
INSERT INTO `sys_role_menu` VALUES (123, 1015);
INSERT INTO `sys_role_menu` VALUES (123, 1016);
INSERT INTO `sys_role_menu` VALUES (123, 1017);
INSERT INTO `sys_role_menu` VALUES (123, 1018);
INSERT INTO `sys_role_menu` VALUES (123, 1019);
INSERT INTO `sys_role_menu` VALUES (123, 1020);
INSERT INTO `sys_role_menu` VALUES (123, 1021);
INSERT INTO `sys_role_menu` VALUES (123, 1022);
INSERT INTO `sys_role_menu` VALUES (123, 1023);
INSERT INTO `sys_role_menu` VALUES (123, 1024);
INSERT INTO `sys_role_menu` VALUES (123, 1025);
INSERT INTO `sys_role_menu` VALUES (123, 1026);
INSERT INTO `sys_role_menu` VALUES (123, 1027);
INSERT INTO `sys_role_menu` VALUES (123, 1028);
INSERT INTO `sys_role_menu` VALUES (123, 1029);
INSERT INTO `sys_role_menu` VALUES (123, 1030);
INSERT INTO `sys_role_menu` VALUES (123, 1031);
INSERT INTO `sys_role_menu` VALUES (123, 1032);
INSERT INTO `sys_role_menu` VALUES (123, 1033);
INSERT INTO `sys_role_menu` VALUES (123, 1034);
INSERT INTO `sys_role_menu` VALUES (123, 1035);
INSERT INTO `sys_role_menu` VALUES (123, 1036);
INSERT INTO `sys_role_menu` VALUES (123, 1037);
INSERT INTO `sys_role_menu` VALUES (123, 1038);
INSERT INTO `sys_role_menu` VALUES (123, 2000);
INSERT INTO `sys_role_menu` VALUES (123, 2001);
INSERT INTO `sys_role_menu` VALUES (123, 2002);
INSERT INTO `sys_role_menu` VALUES (123, 2003);
INSERT INTO `sys_role_menu` VALUES (123, 2004);
INSERT INTO `sys_role_menu` VALUES (123, 2005);
INSERT INTO `sys_role_menu` VALUES (123, 2006);
INSERT INTO `sys_role_menu` VALUES (123, 2007);

-- ----------------------------
-- Table structure for sys_tenant
-- ----------------------------
DROP TABLE IF EXISTS `sys_tenant`;
CREATE TABLE `sys_tenant`  (
  `tenant_code` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '租户编号',
  `tenant_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '租户名称',
  `contact_man` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系人',
  `contact_number` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系电话',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系地址',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '是否删除（0代表存在 2代表删除）',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注说明',
  PRIMARY KEY (`tenant_code`) USING BTREE,
  UNIQUE INDEX `UNIQUE_TENANT_CODE`(`tenant_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_tenant
-- ----------------------------
INSERT INTO `sys_tenant` VALUES ('000000', '平台方', '邓总', '18888888888', '广州市天河区', 'admin', '2019-08-08 15:08:24', 'admin', '2019-12-18 15:25:23', '0', '0', '平台方，系统内置租户，属于基础数据');
INSERT INTO `sys_tenant` VALUES ('111111', '振康物流', '张总', '18888888888', '广州市白云区', 'admin', '2019-07-26 14:58:43', 'admin', '2019-10-21 09:54:42', '0', '0', '0000');
INSERT INTO `sys_tenant` VALUES ('222222', '昆仑科技', '赵总', '16666666666', '广州市越秀区', 'admin', '2019-07-29 14:54:03', NULL, NULL, '0', '0', NULL);
INSERT INTO `sys_tenant` VALUES ('deYdBg', 'ffff', 'tetett', '15112341234', NULL, 'admin', '2019-12-17 19:57:43', NULL, NULL, '0', '0', NULL);
INSERT INTO `sys_tenant` VALUES ('DPoEhI', 'grgrg', 'rgrgr', '15112341234', 'ggeg', 'admin', '2019-12-17 19:56:44', NULL, NULL, '0', '0', 'etetet');
INSERT INTO `sys_tenant` VALUES ('ElRetC', 'AIBDGO', '严荣兴', '13699132623', '13699132623', 'admin', '2019-12-28 17:40:50', NULL, NULL, '0', '0', '13699132623');
INSERT INTO `sys_tenant` VALUES ('FQPIsh', '11', '123', '13022223333', '1231', 'admin', '2019-11-23 16:47:30', NULL, NULL, '0', '0', '231');
INSERT INTO `sys_tenant` VALUES ('gJXFPQ', '123', '111', '13112341234', NULL, 'admin', '2019-11-26 15:34:59', NULL, NULL, '0', '0', NULL);
INSERT INTO `sys_tenant` VALUES ('IVfzVd', 'tetet', 'etetet', '13112212121', 'tetetet', 'admin', '2019-12-17 19:57:15', NULL, NULL, '0', '0', 'etetetet');
INSERT INTO `sys_tenant` VALUES ('mccgAp', 'tttr', 'etetet', '13112132123', 'etetet', 'admin', '2019-12-17 19:58:13', NULL, NULL, '0', '0', NULL);
INSERT INTO `sys_tenant` VALUES ('uDLBWt', '租户试验平台', '碧海1', '18653140396', NULL, 'admin', '2019-11-25 11:35:10', 'admin', '2019-12-17 19:58:27', '0', '0', '');
INSERT INTO `sys_tenant` VALUES ('VBIMDj', '测试租户', 'zzz', '13310005588', NULL, 'admin', '2019-12-12 14:49:41', NULL, NULL, '0', '0', NULL);
INSERT INTO `sys_tenant` VALUES ('xtvXeH', '厦门新星科技', 'Jack', '18288888888', NULL, 'admin', '2019-12-06 17:00:13', 'admin', '2019-12-06 17:00:37', '0', '0', '');

-- ----------------------------
-- Table structure for sys_tenant_application
-- ----------------------------
DROP TABLE IF EXISTS `sys_tenant_application`;
CREATE TABLE `sys_tenant_application`  (
  `tenant_code` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `app_code` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`tenant_code`, `app_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_tenant_application
-- ----------------------------
INSERT INTO `sys_tenant_application` VALUES ('111111', 'TenantApi');
INSERT INTO `sys_tenant_application` VALUES ('111111', 'TenantLog');
INSERT INTO `sys_tenant_application` VALUES ('111111', 'TenantMessage');
INSERT INTO `sys_tenant_application` VALUES ('222222', 'TenantApi');
INSERT INTO `sys_tenant_application` VALUES ('222222', 'TenantBasedate');
INSERT INTO `sys_tenant_application` VALUES ('222222', 'TenantLog');
INSERT INTO `sys_tenant_application` VALUES ('222222', 'TenantMessage');
INSERT INTO `sys_tenant_application` VALUES ('ElRetC', 'TenantApi');
INSERT INTO `sys_tenant_application` VALUES ('ElRetC', 'TenantBasedate');
INSERT INTO `sys_tenant_application` VALUES ('ElRetC', 'TenantMessage');
INSERT INTO `sys_tenant_application` VALUES ('uDLBWt', 'TenantBasedate');
INSERT INTO `sys_tenant_application` VALUES ('uDLBWt', 'TenantLog');
INSERT INTO `sys_tenant_application` VALUES ('xtvXeH', 'TenantBasedate');
INSERT INTO `sys_tenant_application` VALUES ('xtvXeH', 'TenantLog');
INSERT INTO `sys_tenant_application` VALUES ('xtvXeH', 'TenantMessage');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` int(16) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` int(16) NULL DEFAULT NULL COMMENT '部门ID',
  `login_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登录账号',
  `user_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户昵称',
  `tenant_code` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '000000' COMMENT '所属租户',
  `user_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '用户类型（0平台用户，1租户用户）',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '头像路径',
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '盐加密',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '最后登陆IP',
  `login_date` datetime(0) NULL DEFAULT NULL COMMENT '最后登陆时间',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 124 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 103, 'admin', 'vean', '000000', '0', 'd.vean@163.com', '18875125553', '0', '/profile/avatar/2019/12/18/c29ca678bb1c7b71a8c13d9ff8e65f77.png', '29c67a30398638269fe600f73a054934', '111111', '0', '0', '127.0.0.1', '2019-12-31 10:14:09', 'admin', '2018-03-16 11:33:00', 'admin', '2019-12-31 10:14:08', '管理员');
INSERT INTO `sys_user` VALUES (2, 105, 'vean', '维恩', '000000', '0', 'd.vean@qq.com', '15666666666', '1', '', '827ef9e21b74c9b81118265e8290c7de', '90fd46', '0', '2', '127.0.0.1', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', 'admin', '2019-07-30 21:40:15', '测试员');
INSERT INTO `sys_user` VALUES (101, 200, '222222_admin', '222222_admin', '222222', '1', '', '16666666666', '0', '', 'd3a3f1d52cd2b74feb30e8bf007bd376', 'f764c9', '0', '0', '127.0.0.1', '2019-08-16 15:44:44', 'admin', '2019-08-05 10:45:28', '', '2019-12-30 16:09:54', NULL);
INSERT INTO `sys_user` VALUES (103, 202, '111111_admin', '111111_admin', '111111', '1', '', '18888888888', '0', '', '8459be1de6edd7a824e5670041a892a6', '4f245f', '0', '0', '192.168.2.3', '2019-12-24 11:38:32', 'admin', '2019-08-06 18:23:53', '', '2019-12-24 11:38:32', NULL);
INSERT INTO `sys_user` VALUES (104, 203, 'test', 'test', '111111', '1', 'test@163.com', '16766666666', '0', '', '3c545812bef15e24627f253b3d64077f', '28c037', '0', '0', '192.168.2.3', '2019-12-25 16:15:07', '111111_admin', '2019-08-13 09:57:20', 'admin', '2019-12-25 16:15:07', '');
INSERT INTO `sys_user` VALUES (105, 204, 'test_02', 'test', '222222', '1', 'test@126.com', '18766666666', '0', '', '5650e74ac22e09910d8c7daabeb74a5a', '19969d', '0', '0', '127.0.0.1', '2019-11-23 16:35:46', '222222_admin', '2019-08-14 11:02:17', '', '2019-11-23 16:35:45', NULL);
INSERT INTO `sys_user` VALUES (106, 203, 'test11', 'test11', '111111', '1', 'test@1613.com', '16711166666', '0', '', '66630db654251c660bbc0da263845de4', '017d09', '0', '2', '', NULL, '111111_admin', '2019-08-14 14:25:51', '', NULL, NULL);
INSERT INTO `sys_user` VALUES (107, 103, '111111', '1111111111', '000000', '1', 'dest@163.com', '16766666667', '0', '', '22be1a8e3beb239f71850aa5216b8c3c', '1b03a3', '0', '0', '', NULL, 'admin', '2019-08-16 14:37:38', '', NULL, NULL);
INSERT INTO `sys_user` VALUES (111, 103, 'te', 'te', '000000', '1', '1222@11.com', '13693220391', '0', '', '56acf4669daa46139882f340204825f1', 'ae819b', '0', '2', '', NULL, 'admin', '2019-10-24 15:34:59', '', NULL, NULL);
INSERT INTO `sys_user` VALUES (112, 207, 't1', 't1', '111111', '1', '12221@11.com', '13693220392', '0', '', '541090bdaba5346258fa346fd495d506', '66e615', '0', '2', '192.168.2.3', '2019-10-24 16:32:24', 'test', '2019-10-24 15:46:57', '', '2019-10-24 16:32:23', NULL);
INSERT INTO `sys_user` VALUES (113, NULL, 't11', 't11', '111111', '1', '106661641@qq.com', '13693220395', '0', '', '6ffc3cb49b2e533e77087da0113c284a', 'e0e97d', '0', '2', '192.168.2.3', '2019-10-24 16:23:33', 't1', '2019-10-24 16:21:20', '', '2019-10-24 16:23:33', NULL);
INSERT INTO `sys_user` VALUES (114, 103, 'chenns', 'chenns', '000000', '1', 'chenns@139.com', '18721270334', '0', '', 'df065a7e504c8573bfc8d346b8fabf01', 'b60847', '0', '0', '', NULL, 'admin', '2019-10-24 17:34:20', '', NULL, '123');
INSERT INTO `sys_user` VALUES (116, 207, 'zhan', '詹詹詹', '000000', '1', 'qwe@qq.com', '18627715440', '0', '', '579fb34a0d3d423773614d55313f53f8', '633107', '0', '2', '192.168.2.3', '2019-10-25 15:27:40', 'admin', '2019-10-25 15:27:24', '', '2019-10-25 15:27:40', NULL);
INSERT INTO `sys_user` VALUES (117, 207, 'ruiplan', 'ruiplan', '111111', '1', '333@qq.com', '13800138000', '0', '', 'a70c54cc27bf63bcec4e985f9d84d097', '0cee92', '0', '0', '192.168.2.3', '2019-11-23 16:45:30', 'test', '2019-11-23 16:45:18', '', '2019-11-23 16:45:30', NULL);
INSERT INTO `sys_user` VALUES (118, 212, 'uDLBWt_admin', 'uDLBWt_admin', 'uDLBWt', '1', '', '18653140396', '0', '', '844d9a5c22d9f2b32d2fbf44be7c57f6', '33478b', '0', '0', '', NULL, 'admin', '2019-11-25 11:35:32', '', NULL, NULL);
INSERT INTO `sys_user` VALUES (119, 103, 'testss', 'test', '000000', '1', '4508054@qq.com', '15088137519', '0', '', '7a58cb7c08b8f8509d39b3b8466f157e', '7f17d7', '1', '0', '192.168.2.3', '2019-12-03 18:52:16', 'admin', '2019-12-03 18:49:27', 'admin', '2019-12-06 16:58:51', '');
INSERT INTO `sys_user` VALUES (120, 106, '111', '海涛子', '000000', '1', 'haitao@163.com', '13912345678', '1', '', 'fc3e1b6aa88c61d012c13e5a7a338492', '092509', '0', '0', '', NULL, 'admin', '2019-12-05 11:57:13', 'admin', '2019-12-05 11:57:54', '');
INSERT INTO `sys_user` VALUES (121, 106, 'flb', '傅老板', '000000', '1', 'fu@163.com', '13912345679', '0', '', '766edf6021e5a4c6108f869a1361aa8d', '46df89', '0', '0', '', NULL, 'admin', '2019-12-05 11:59:15', '', NULL, NULL);
INSERT INTO `sys_user` VALUES (122, 216, 'xtvXeH_admin', 'xtvXeH_admin', 'xtvXeH', '1', '', '18288888888', '0', '', '8ba88088a74eeb367ac18cc966f03d08', '1ab94b', '0', '0', '', NULL, 'admin', '2019-12-06 17:01:01', '', NULL, NULL);
INSERT INTO `sys_user` VALUES (123, 219, 'ElRetC_admin', 'ElRetC_admin', 'ElRetC', '1', '', '13699132623', '0', '', '5fbce5c848d7459e87707408dd0f9ccd', '4de92d', '0', '0', '192.168.2.3', '2019-12-28 17:57:17', 'admin', '2019-12-28 17:47:11', '', '2019-12-28 17:57:17', NULL);

-- ----------------------------
-- Table structure for sys_user_online
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_online`;
CREATE TABLE `sys_user_online`  (
  `sessionId` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户会话id',
  `login_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录账号',
  `dept_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `tenant_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租户名称',
  `ipaddr` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(43) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '在线状态on_line在线off_line离线',
  `start_timestamp` datetime(0) NULL DEFAULT NULL COMMENT 'session创建时间',
  `last_access_time` datetime(0) NULL DEFAULT NULL COMMENT 'session最后访问时间',
  `expire_time` int(5) NULL DEFAULT 0 COMMENT '超时时间，单位为分钟',
  PRIMARY KEY (`sessionId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '在线用户记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_online
-- ----------------------------
INSERT INTO `sys_user_online` VALUES ('929f7340-e0a3-4e22-a929-ea5d13a58736', 'admin', '研发部门', NULL, '127.0.0.1', '内网IP', 'Opera', 'Windows 10', 'on_line', '2019-12-31 10:14:05', '2019-12-31 10:14:09', 1800000);

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);
INSERT INTO `sys_user_post` VALUES (2, 2);
INSERT INTO `sys_user_post` VALUES (105, 3);
INSERT INTO `sys_user_post` VALUES (106, 3);
INSERT INTO `sys_user_post` VALUES (112, 8);
INSERT INTO `sys_user_post` VALUES (114, 2);
INSERT INTO `sys_user_post` VALUES (116, 1);
INSERT INTO `sys_user_post` VALUES (117, 8);
INSERT INTO `sys_user_post` VALUES (119, 1);
INSERT INTO `sys_user_post` VALUES (120, 1);
INSERT INTO `sys_user_post` VALUES (121, 1);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` int(16) NOT NULL COMMENT '用户ID',
  `role_id` int(16) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 2);
INSERT INTO `sys_user_role` VALUES (101, 100);
INSERT INTO `sys_user_role` VALUES (103, 102);
INSERT INTO `sys_user_role` VALUES (104, 102);
INSERT INTO `sys_user_role` VALUES (105, 100);
INSERT INTO `sys_user_role` VALUES (106, 108);
INSERT INTO `sys_user_role` VALUES (107, 104);
INSERT INTO `sys_user_role` VALUES (111, 100);
INSERT INTO `sys_user_role` VALUES (111, 112);
INSERT INTO `sys_user_role` VALUES (112, 112);
INSERT INTO `sys_user_role` VALUES (113, 112);
INSERT INTO `sys_user_role` VALUES (114, 1);
INSERT INTO `sys_user_role` VALUES (114, 2);
INSERT INTO `sys_user_role` VALUES (116, 102);
INSERT INTO `sys_user_role` VALUES (117, 102);
INSERT INTO `sys_user_role` VALUES (118, 1);
INSERT INTO `sys_user_role` VALUES (118, 118);
INSERT INTO `sys_user_role` VALUES (119, 119);
INSERT INTO `sys_user_role` VALUES (119, 121);
INSERT INTO `sys_user_role` VALUES (120, 1);
INSERT INTO `sys_user_role` VALUES (120, 2);
INSERT INTO `sys_user_role` VALUES (121, 1);
INSERT INTO `sys_user_role` VALUES (121, 2);
INSERT INTO `sys_user_role` VALUES (122, 120);
INSERT INTO `sys_user_role` VALUES (123, 123);

-- ----------------------------
-- Table structure for test
-- ----------------------------
DROP TABLE IF EXISTS `test`;
CREATE TABLE `test`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of test
-- ----------------------------
INSERT INTO `test` VALUES (9, '320134');
INSERT INTO `test` VALUES (10, '222');
INSERT INTO `test` VALUES (12, '生成有用的东东看看啊');
INSERT INTO `test` VALUES (13, '1');
INSERT INTO `test` VALUES (14, '埃维昂无');
INSERT INTO `test` VALUES (15, '办公用品管理');
INSERT INTO `test` VALUES (16, '生成测试');
INSERT INTO `test` VALUES (17, '江苏卫生监督');
INSERT INTO `test` VALUES (18, 'apt.flopSchd.output');
INSERT INTO `test` VALUES (19, '华为');
INSERT INTO `test` VALUES (20, 'www');
INSERT INTO `test` VALUES (21, 'test0091');

SET FOREIGN_KEY_CHECKS = 1;
